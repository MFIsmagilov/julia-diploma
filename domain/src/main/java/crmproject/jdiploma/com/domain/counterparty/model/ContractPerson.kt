package crmproject.jdiploma.com.domain.counterparty.model

import crmproject.jdiploma.com.domain.counterparty.model.Counterparty

/**
 * Created by MaRaT on 03.06.2018.
 */
class ContractPerson(
        val id: Int,
        val contractingParties: Counterparty,
        val SPN: String,
        val email: String,
        val phone: String,
        val positionFace: String
)