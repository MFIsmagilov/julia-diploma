package crmproject.jdiploma.com.domain.user.interactor.browse

import crmproject.jdiploma.com.domain.user.interactor.UserSingleUseCase
import crmproject.jdiploma.com.domain.user.repository.UserRepository
import crmproject.jdiploma.com.domain.executor.PostExecutionThread
import crmproject.jdiploma.com.domain.executor.ThreadExecutor
import crmproject.jdiploma.com.domain.user.model.Registration
import crmproject.jdiploma.com.domain.user.user.User
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
open class UserBrowse @Inject constructor(private val userRepository: UserRepository,
                                          threadExecutor: ThreadExecutor,
                                          postExecutionThread: PostExecutionThread
):
        UserSingleUseCase<User, Int>(threadExecutor, postExecutionThread) {
    override fun buildLoadUserObservable(): Single<List<User>> {
        return userRepository.getUsers()
    }

    override fun registerUserCase(registration: Registration): Single<User> {
        return userRepository.registerUser(registration)
    }

    public override fun buildUseCaseObservable(params: Int): Single<User> {
        return userRepository.getUser(params)
    }


}