package crmproject.jdiploma.com.domain.counterparty.interactor.browse

import crmproject.jdiploma.com.domain.counterparty.interactor.CouterpartySingleUseCase
import crmproject.jdiploma.com.domain.counterparty.model.Counterparty
import crmproject.jdiploma.com.domain.counterparty.repository.ContractingPartiesRepository
import crmproject.jdiploma.com.domain.executor.PostExecutionThread
import crmproject.jdiploma.com.domain.executor.ThreadExecutor
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
open class CountingPartiesBrowse @Inject constructor(
        private val cotractingPartiesRepository: ContractingPartiesRepository,
        threadExecutor: ThreadExecutor,
        postExecutionThread: PostExecutionThread
) : CouterpartySingleUseCase<Counterparty, Void>(threadExecutor, postExecutionThread) {

    override fun buildLoadContractingPartiesObservable(): Single<List<Counterparty>> {
        return cotractingPartiesRepository.getContractingParties()
    }
}