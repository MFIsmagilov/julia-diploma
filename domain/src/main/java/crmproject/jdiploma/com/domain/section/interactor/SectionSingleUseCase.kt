package crmproject.jdiploma.com.domain.section.interactor

import crmproject.jdiploma.com.domain.BaseSingleUseCase
import crmproject.jdiploma.com.domain.executor.PostExecutionThread
import crmproject.jdiploma.com.domain.executor.ThreadExecutor

/**
 * Created by MaRaT on 05.03.2018.
 */
abstract class SectionSingleUseCase<T, in Params> constructor(
        threadExecutor: ThreadExecutor,
        postExecutionThread: PostExecutionThread
) : BaseSingleUseCase<T, Params>(
        threadExecutor,
        postExecutionThread
)