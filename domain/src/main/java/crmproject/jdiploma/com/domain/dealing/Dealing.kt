package crmproject.jdiploma.com.domain.dealing

import crmproject.jdiploma.com.domain.counterparty.model.Counterparty
import crmproject.jdiploma.com.domain.user.user.User
import java.util.*

/**
 * Created by MaRaT on 15.03.2018.
 */
class Dealing(
        val user: User,
        val counterparty: Counterparty,
        val name: String,
        val dateBeginning: Date,
        val dateCompletition: Date,
        val summa: Int
)