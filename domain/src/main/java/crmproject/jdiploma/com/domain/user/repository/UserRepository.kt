package crmproject.jdiploma.com.domain.user.repository

import crmproject.jdiploma.com.domain.user.model.Registration
import crmproject.jdiploma.com.domain.user.user.User
import io.reactivex.Single

/**
 * Created by MaRaT on 28.02.2018.
 */

interface UserRepository {
    fun getUsers(): Single<List<User>>

    fun getUser(id: Int): Single<User>

    fun registerUser(registerData: Registration): Single<User>
}