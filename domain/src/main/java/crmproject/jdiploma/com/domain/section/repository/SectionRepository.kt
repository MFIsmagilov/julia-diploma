package crmproject.jdiploma.com.domain.section.repository

import crmproject.jdiploma.com.domain.section.model.Section
import io.reactivex.Single

/**
 * Created by MaRaT on 05.03.2018.
 */
interface SectionRepository {

    fun getSections(): Single<List<Section>>

}