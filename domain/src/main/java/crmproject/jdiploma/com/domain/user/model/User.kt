package crmproject.jdiploma.com.domain.user.user

import crmproject.jdiploma.com.domain.organization.Organization
import crmproject.jdiploma.com.domain.role.Role
import java.util.*

/**
 * Created by MaRaT on 28.02.2018.
 */
class DjangoUser(
        val id: Int,
        val username: String,
        val firstName: String,
        val lastName: String,
        val email: String,
        val isSuperuser: Boolean,
        val lastLogin: Date?
)

class User(
        val id: Int,
        val djangoUser: DjangoUser,
        val role: Role,
        val organization: Organization,
        val position: String
)