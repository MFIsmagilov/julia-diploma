package crmproject.jdiploma.com.domain.task.interactor

import crmproject.jdiploma.com.domain.executor.PostExecutionThread
import crmproject.jdiploma.com.domain.executor.ThreadExecutor
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers

/**
 * Created by MaRaT on 07.03.2018.
 */
abstract class TaskUseCase<T, in Params> constructor(
        private val threadExecutor: ThreadExecutor,
        private val postExecutionThread: PostExecutionThread) {

    private val disposables = CompositeDisposable()

    protected abstract fun buildLoadTasksUseCaseObservable(): Single<List<T>>

    protected abstract fun buildCreateTasksUseCaseObservable(task: T): Single<T>

    open fun loadTask(singleObserver: DisposableSingleObserver<List<T>>) {
        val single = this.buildLoadTasksUseCaseObservable()
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler) as Single<List<T>>
        addDisposable(single.subscribeWith(singleObserver))
    }

    open fun createTask(singleObserver: DisposableSingleObserver<T>, params: T){
        val single = this.buildCreateTasksUseCaseObservable(params)
                .subscribeOn(Schedulers.from(threadExecutor))
                .observeOn(postExecutionThread.scheduler) as Single<T>
        addDisposable(single.subscribeWith(singleObserver))
    }

    fun dispose() {
        if (!disposables.isDisposed) disposables.dispose()
    }

    private fun addDisposable(disposable: Disposable) {
        disposables.add(disposable)
    }

}