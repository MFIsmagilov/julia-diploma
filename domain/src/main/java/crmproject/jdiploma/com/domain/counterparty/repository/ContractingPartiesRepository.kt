package crmproject.jdiploma.com.domain.counterparty.repository

import crmproject.jdiploma.com.domain.counterparty.model.Counterparty
import io.reactivex.Single

/**
 * Created by MaRaT on 03.06.2018.
 */
interface ContractingPartiesRepository {
    fun getContractingParties(): Single<List<Counterparty>>
}