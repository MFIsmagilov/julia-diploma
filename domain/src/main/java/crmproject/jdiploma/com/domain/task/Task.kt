package crmproject.jdiploma.com.domain.task

import crmproject.jdiploma.com.domain.user.user.User
import java.util.*

/**
 * Created by MaRaT on 05.03.2018.
 */
class Task(
        val appoinmentDate: Date,
        val dateCompletion: Date,
        val comment: String,
        val user: User?,
        val status: String
)