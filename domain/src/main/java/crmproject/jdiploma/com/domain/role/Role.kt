package crmproject.jdiploma.com.domain.role

/**
 * Created by MaRaT on 28.02.2018.
 */
class Role(
        val id: Int,
        val name: String
)