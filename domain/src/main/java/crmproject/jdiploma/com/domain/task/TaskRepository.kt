package crmproject.jdiploma.com.domain.task

import io.reactivex.Single

/**
 * Created by MaRaT on 07.03.2018.
 */
interface TaskRepository {

    fun getTasks(): Single<List<Task>>

    fun createTask(task: Task): Single<Task>

}