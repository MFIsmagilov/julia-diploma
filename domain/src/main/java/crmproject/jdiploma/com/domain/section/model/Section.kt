package crmproject.jdiploma.com.domain.section.model

/**
 * Created by MaRaT on 05.03.2018.
 */
class Section(
        val nameSec: String,
        val linkSection: String,
        val isDefault: Boolean
)