package crmproject.jdiploma.com.domain.section.interactor.browse

import crmproject.jdiploma.com.domain.executor.PostExecutionThread
import crmproject.jdiploma.com.domain.executor.ThreadExecutor
import crmproject.jdiploma.com.domain.section.interactor.SectionSingleUseCase
import crmproject.jdiploma.com.domain.section.model.Section
import crmproject.jdiploma.com.domain.section.repository.SectionRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class SectionBrowse @Inject constructor(private val sectionRepository: SectionRepository,
                                        threadExecutor: ThreadExecutor,
                                        postExecutionThread: PostExecutionThread
) : SectionSingleUseCase<List<Section>, Void?>(threadExecutor, postExecutionThread) {

    override fun buildUseCaseObservable(params: Void?): Single<List<Section>> {
        return sectionRepository.getSections()
    }

}