package crmproject.jdiploma.com.domain.counterparty.repository

import crmproject.jdiploma.com.domain.counterparty.model.ContractPerson
import io.reactivex.Single

/**
 * Created by MaRaT on 03.06.2018.
 */
interface ContractPersonRepository {

    fun getContractPerson(): Single<ContractPerson>

}