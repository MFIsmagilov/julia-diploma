package crmproject.jdiploma.com.domain

import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

/**
 * Created by MaRaT on 28.02.2018.
 */
open class BaseSingleObserver<T> : SingleObserver<T> {

    override fun onSubscribe(d: Disposable) { }

    override fun onSuccess(t: T) { }

    override fun onError(exception: Throwable) { }

}