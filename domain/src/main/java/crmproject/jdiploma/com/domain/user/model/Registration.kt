package crmproject.jdiploma.com.domain.user.model

/**
 * Created by MaRaT on 28.02.2018.
 */
class Registration(
        val nameOrganization: String,
        val siteOrganization: String,
        val username: String,
        val password: String,
        val confirmPassword: String
)