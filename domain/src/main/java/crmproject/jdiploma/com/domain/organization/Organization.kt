package crmproject.jdiploma.com.domain.organization

/**
 * Created by MaRaT on 28.02.2018.
 */
class Organization(
        val id: Int,
        val name: String,
        val site: String
)