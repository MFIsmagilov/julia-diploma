package crmproject.jdiploma.com.domain.task.interactor.browse

import crmproject.jdiploma.com.domain.executor.PostExecutionThread
import crmproject.jdiploma.com.domain.executor.ThreadExecutor
import crmproject.jdiploma.com.domain.task.Task
import crmproject.jdiploma.com.domain.task.TaskRepository
import crmproject.jdiploma.com.domain.task.interactor.TaskUseCase
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 07.03.2018.
 */
open class TaskBrowse @Inject constructor(private val taskRepository: TaskRepository,
                                          threadExecutor: ThreadExecutor,
                                          postExecutionThread: PostExecutionThread
) : TaskUseCase<Task, Int>(threadExecutor, postExecutionThread) {
    override fun buildCreateTasksUseCaseObservable(task: Task): Single<Task> {
        return taskRepository.createTask(task)
    }

    override fun buildLoadTasksUseCaseObservable(): Single<List<Task>> {
        return taskRepository.getTasks()
    }
}