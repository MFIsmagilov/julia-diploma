package crmproject.jdiploma.com.domain.executor

import io.reactivex.Scheduler

/**
 * Created by MaRaT on 28.02.2018.
 */
interface PostExecutionThread {
    val scheduler: Scheduler
}