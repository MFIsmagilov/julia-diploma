package crmproject.jdiploma.com.domain.counterparty.model

/**
 * Created by MaRaT on 03.06.2018.
 */
class Counterparty(
        val id: Int,
        val name: String,
        val address: String,
        val site: String
)