package crmproject.jdiploma.com.domain.executor

import java.util.concurrent.Executor

/**
 * Created by MaRaT on 28.02.2018.
 */

interface ThreadExecutor: Executor
