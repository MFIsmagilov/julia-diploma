package crmproject.jdiploma.com.remote.models.dealing

import crmproject.jdiploma.com.remote.models.counterparty.ContractingPartiesModel
import crmproject.jdiploma.com.remote.models.user.UserModel
import java.util.*

/**
 * Created by MaRaT on 25.02.2018.
 */
class DealingModel(
        val idUser: UserModel,
        val idCP: ContractingPartiesModel,
        val nameDealing: String,
        val dateBeginning: Date,
        val dateCompletition: Date,
        val summa: Int
)