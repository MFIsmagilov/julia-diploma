package crmproject.jdiploma.com.remote.models.dealing.mappers.impls

import crmproject.jdiploma.com.data.dealing.DealingEntity
import crmproject.jdiploma.com.data.dealing.mapper.DealingMapper
import crmproject.jdiploma.com.data.dealing.repository.DealingRemote
import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.mappers.DealingEntityMappers
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 15.03.2018.
 */
//class DealingRemoteImpl @Inject constructor(
//        private val crmService: CrmService,
//        private val dealingMapper: DealingEntityMappers
//): DealingRemote{
//    override fun getDealings(): Single<List<DealingEntity>> {
//        return crmService.getDealings()
//                .map {
//                    it.map{
//                        dealingMapper.m(it)
//                    }
//                }
//    }
//
//}