package crmproject.jdiploma.com.remote.models.user.mappers

import crmproject.jdiploma.com.data.user.models.DjangoUserEntity
import crmproject.jdiploma.com.data.user.models.UserEntity
import crmproject.jdiploma.com.remote.mappers.EntityMapper
import crmproject.jdiploma.com.remote.mappers.OrganizationEntityMapper
import crmproject.jdiploma.com.remote.mappers.RoleEntityMapper
import crmproject.jdiploma.com.remote.models.user.DjangoUserModel
import crmproject.jdiploma.com.remote.models.user.UserModel

import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */

open class DjangoUserEntityMapper @Inject constructor() : EntityMapper<DjangoUserModel, DjangoUserEntity> {
    override fun mapToRemote(type: DjangoUserEntity): DjangoUserModel {
        return DjangoUserModel(
                type.id,
                type.username,
                type.firstName,
                type.lastName,
                type.email,
                type.isSuperuser,
                type.lastLogin
        )
    }

    override fun mapFromRemote(type: DjangoUserModel): DjangoUserEntity {
        return DjangoUserEntity(
                type.id,
                type.username,
                type.first_name,
                type.last_name,
                type.email,
                type.is_superuser,
                type.last_login
        )
    }

}


open class UserEntityMapper @Inject constructor(
        private val djangoUserMapper: DjangoUserEntityMapper,
        private val roleMapper: RoleEntityMapper,
        private val organizationMapper: OrganizationEntityMapper
) : EntityMapper<UserModel, UserEntity> {
    override fun mapToRemote(type: UserEntity): UserModel {
        return UserModel(
                type.id,
                djangoUserMapper.mapToRemote(type.djangoUser),
                roleMapper.mapToRemote(type.role),
                organizationMapper.mapToRemote(type.organization),
                type.position
        )
    }

    /**
     * Map an instance of a [BufferooModel] to a [BufferooEntity] model
     */
    override fun mapFromRemote(type: UserModel): UserEntity {
        return UserEntity(
                type.id,
                djangoUserMapper.mapFromRemote(type.idUser),
                roleMapper.mapFromRemote(type.idRole),
                organizationMapper.mapFromRemote(type.idOrg),
                type.positionUser
        )
    }
}