package crmproject.jdiploma.com.remote.models.user

import crmproject.jdiploma.com.remote.models.organization.OrganizationModel
import crmproject.jdiploma.com.remote.models.role.RoleModel
import java.util.*

/**
 * Created by MaRaT on 25.02.2018.
 */
class DjangoUserModel(
        val id: Int,
        val username: String,
        val first_name: String,
        val last_name: String,
        val email: String,
        val is_superuser: Boolean,
        val last_login: Date?
)

class UserModel(
        val id: Int,
        val idUser: DjangoUserModel,
        val idRole: RoleModel,
        val idOrg: OrganizationModel,
        val positionUser: String
)