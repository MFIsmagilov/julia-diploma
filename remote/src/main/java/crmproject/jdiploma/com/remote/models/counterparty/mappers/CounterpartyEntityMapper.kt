package crmproject.jdiploma.com.remote.models.counterparty.mappers

import crmproject.jdiploma.com.data.counterparty.models.CounterpartyEntity
import crmproject.jdiploma.com.data.task.model.TaskEntity
import crmproject.jdiploma.com.remote.mappers.EntityMapper
import crmproject.jdiploma.com.remote.models.counterparty.ContractingPartiesModel
import crmproject.jdiploma.com.remote.models.task.TaskModel
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
open class CounterpartyEntityMapper @Inject constructor(
) : EntityMapper<ContractingPartiesModel, CounterpartyEntity> {
    override fun mapFromRemote(type: ContractingPartiesModel): CounterpartyEntity {
        return CounterpartyEntity(
                type.id,
                type.nameCP,
                type.addressCP,
                type.siteCP
        )
    }

    override fun mapToRemote(type: CounterpartyEntity): ContractingPartiesModel {
        return ContractingPartiesModel(
                type.id,
                type.name,
                type.address,
                type.site
        )
    }
}