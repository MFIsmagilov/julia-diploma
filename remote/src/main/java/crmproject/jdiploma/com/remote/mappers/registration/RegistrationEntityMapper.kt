package crmproject.jdiploma.com.remote.mappers.registration

import crmproject.jdiploma.com.data.user.models.RegistrationEntity
import crmproject.jdiploma.com.remote.mappers.EntityMapper
import crmproject.jdiploma.com.remote.models.user.registration.RegistrationModel
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
open class RegistrationEntityMapper @Inject constructor() : EntityMapper<RegistrationModel, RegistrationEntity> {
    override fun mapToRemote(type: RegistrationEntity): RegistrationModel {
        return RegistrationModel(
                type.nameOrganization,
                type.siteOrganization,
                type.username,
                type.password,
                type.confirmPassword
        )
    }

    override fun mapFromRemote(type: RegistrationModel): RegistrationEntity {
        return RegistrationEntity(
                type.nameOrganization,
                type.siteOrganization,
                type.username,
                type.password,
                type.confirmPassword
        )
    }
}