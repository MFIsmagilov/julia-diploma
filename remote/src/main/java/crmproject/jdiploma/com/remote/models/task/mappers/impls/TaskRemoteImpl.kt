package crmproject.jdiploma.com.remote.models.task.mappers.impls

import crmproject.jdiploma.com.data.task.model.TaskEntity
import crmproject.jdiploma.com.data.task.repository.TaskRemote
import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.models.task.mappers.TaskEntityMapper
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class TaskRemoteImpl @Inject constructor(
        private val crmService: CrmService,
        private val taskEntityMapper: TaskEntityMapper
) : TaskRemote {
    override fun getTasks(): Single<List<TaskEntity>> {
        return crmService.getTasks()
                .map {
                    it.map {
                        taskEntityMapper.mapFromRemote(it)
                    }
                }
    }

    override fun createTask(taskEntity: TaskEntity): Single<TaskEntity> {
        return crmService
                .createTask(taskEntityMapper.mapToRemote(taskEntity))
                .map {
                    taskEntityMapper.mapFromRemote(it)
                }
    }
}