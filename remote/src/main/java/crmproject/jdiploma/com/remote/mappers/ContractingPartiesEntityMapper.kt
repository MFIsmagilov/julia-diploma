package crmproject.jdiploma.com.remote.mappers

//import crmproject.jdiploma.com.data.counterparty.models.CounterpartyEntity
import crmproject.jdiploma.com.remote.models.counterparty.ContractingPartiesModel
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
//open class ContractingPartiesEntityMapper @Inject constructor(): EntityMapper<ContractingPartiesModel, CounterpartyEntity>{
//    override fun mapToRemote(type: CounterpartyEntity): ContractingPartiesModel {
//        return ContractingPartiesModel(
//                type.name,
//                type.address,
//                type.site
//        )
//    }
//
//    override fun mapFromRemote(type: ContractingPartiesModel): CounterpartyEntity {
//        return CounterpartyEntity(
//                type.nameCP,
//                type.addressCP,
//                type.siteCP
//        )
//    }
//}