package crmproject.jdiploma.com.remote.models.section

/**
 * Created by MaRaT on 25.02.2018.
 */
class SectionModel(
        val nameSec: String,
        val linkSection: String,
        val isDefault: Boolean
)