package crmproject.jdiploma.com.remote.models.counterparty

/**
 * Created by MaRaT on 25.02.2018.
 */
class ContractingPartiesModel(
        val id: Int,
        val nameCP: String,
        val addressCP: String,
        val siteCP: String
)