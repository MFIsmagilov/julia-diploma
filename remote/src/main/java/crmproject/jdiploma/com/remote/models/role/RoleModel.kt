package crmproject.jdiploma.com.remote.models.role

/**
 * Created by MaRaT on 25.02.2018.
 */
class RoleModel(
        val id: Int,
        val nameRole: String
)