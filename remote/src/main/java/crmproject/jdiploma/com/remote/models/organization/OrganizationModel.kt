package crmproject.jdiploma.com.remote.models.organization

/**
 * Created by MaRaT on 25.02.2018.
 */
class OrganizationModel(
        val id: Int,
        val nameOrganization: String,
        val siteOrganization: String
)