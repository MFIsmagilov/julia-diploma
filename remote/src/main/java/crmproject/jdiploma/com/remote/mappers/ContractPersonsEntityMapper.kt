package crmproject.jdiploma.com.remote.mappers

//import crmproject.jdiploma.com.data.counterparty.ContractPersonsEntity
import crmproject.jdiploma.com.remote.models.counterparty.ContractPersonsModel
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
//open class ContractPersonsEntityMapper @Inject constructor(
//        private val contractingPartiesEntityMapper: ContractingPartiesEntityMapper
//) : EntityMapper<ContractPersonsModel, ContractPersonsEntity> {
//    override fun mapToRemote(type: ContractPersonsEntity): ContractPersonsModel {
//        return ContractPersonsModel(
//                contractingPartiesEntityMapper.mapToRemote(type.idCP),
//                type.SPN,
//                type.email,
//                type.phone,
//                type.positinFace
//        )
//    }
//
//    override fun mapFromRemote(type: ContractPersonsModel): ContractPersonsEntity {
//        return ContractPersonsEntity(
//                contractingPartiesEntityMapper.mapFromRemote(type.idCP),
//                type.SPN,
//                type.email,
//                type.phone,
//                type.positinFace
//        )
//    }
//}