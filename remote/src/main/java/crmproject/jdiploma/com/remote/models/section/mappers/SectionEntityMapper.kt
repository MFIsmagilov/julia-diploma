package crmproject.jdiploma.com.remote.models.section.mappers

import crmproject.jdiploma.com.data.section.model.SectionEntity
import crmproject.jdiploma.com.remote.mappers.EntityMapper
import crmproject.jdiploma.com.remote.models.section.SectionModel
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class SectionEntityMapper @Inject constructor() : EntityMapper<SectionModel, SectionEntity> {
    override fun mapFromRemote(type: SectionModel): SectionEntity {
        return SectionEntity(
                type.nameSec,
                type.linkSection,
                type.isDefault
        )
    }

    override fun mapToRemote(type: SectionEntity): SectionModel {
        return SectionModel(
                type.name,
                type.linkSection,
                type.isDefault
        )
    }
}