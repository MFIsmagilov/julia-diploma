package crmproject.jdiploma.com.remote.models.dealing.mappers

import crmproject.jdiploma.com.data.dealing.DealingEntity
import crmproject.jdiploma.com.data.dealing.mapper.DealingMapper
import crmproject.jdiploma.com.remote.mappers.EntityMapper
import crmproject.jdiploma.com.remote.models.dealing.DealingModel
import crmproject.jdiploma.com.remote.models.user.mappers.UserEntityMapper
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
//class DealingEntityMapper @Inject constructor(
//        private val userEntityMapper: UserEntityMapper,
//) : EntityMapper<DealingModel, DealingEntity> {
//    override fun mapFromRemote(type: DealingModel): DealingEntity {
//        return DealingEntity(
//                userEntityMapper.mapFromRemote(type.idUser),
//                t
//        )
//    }
//
//    override fun mapToRemote(type: DealingEntity): DealingModel {
//        return dealingMapper.mapToRemote(type)
//    }
//
//}