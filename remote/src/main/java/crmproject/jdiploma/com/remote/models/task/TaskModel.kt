package crmproject.jdiploma.com.remote.models.task

import crmproject.jdiploma.com.remote.models.user.UserModel
import java.util.*

/**
 * Created by MaRaT on 25.02.2018.
 */
class TaskModel(
        val appoinmentDate: Date,
        val dateCompletion: Date,
        val comment: String,
        val idUser: List<UserModel>,
        val status: String
)