package crmproject.jdiploma.com.remote.mappers

/**
 * Created by MaRaT on 15.03.2018.
 */
class CounterpartyModel(
        val id: Int,
        val name: String,
        val address: String,
        val site: String,
        val SPN: String,
        val email: String,
        val phone: String,
        val positionFace: String
)