package crmproject.jdiploma.com.remote.mappers

/**
 * Created by MaRaT on 25.02.2018.
 */
interface EntityMapper<M, E> {

    fun mapFromRemote(type: M): E

    fun mapToRemote(type: E): M
}