package crmproject.jdiploma.com.remote.models.counterparty.mappers.impl

import crmproject.jdiploma.com.data.counterparty.models.CounterpartyEntity
import crmproject.jdiploma.com.data.counterparty.repository.ContractingPartiesRemote
import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.models.counterparty.mappers.CounterpartyEntityMapper
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class ContractingPartiesRemoteImpl @Inject constructor(
        private val crmService: CrmService,
        private val mapper: CounterpartyEntityMapper
) : ContractingPartiesRemote {
    override fun getContractingParties(): Single<List<CounterpartyEntity>> {
        return crmService.getContractingParties()
                .map {
                    it.map {
                        mapper.mapFromRemote(it)
                    }
                }
    }
}