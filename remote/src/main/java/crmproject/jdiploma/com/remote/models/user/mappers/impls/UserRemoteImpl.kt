package crmproject.jdiploma.com.remote.models.user.mappers.impls

import crmproject.jdiploma.com.data.user.models.UserEntity
import crmproject.jdiploma.com.data.user.models.RegistrationEntity
import crmproject.jdiploma.com.data.user.repository.UserRemote
import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.models.user.mappers.UserEntityMapper
import crmproject.jdiploma.com.remote.mappers.registration.RegistrationEntityMapper
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
class UserRemoteImpl @Inject constructor(
        private val crmService: CrmService,
        private val userEntityMapper: UserEntityMapper,
        private val registrationEntityMapper: RegistrationEntityMapper
) : UserRemote {

    override fun getUsers(): Single<List<UserEntity>> {
        return crmService.getUsers()
                .map {
                    it.map { user ->
                        userEntityMapper.mapFromRemote(user)
                    }
                }
    }

    override fun getUser(id: Int): Single<UserEntity> {
        return crmService.getUser(id)
                .map {
                    userEntityMapper.mapFromRemote(it)
                }
    }

    override fun registrationUser(obj: RegistrationEntity): Single<UserEntity> {
        val registrationModel = registrationEntityMapper.mapToRemote(obj)
        return crmService.registrationUser(registrationModel)
                .map {
                    userEntityMapper.mapFromRemote(it)
                }
    }
}