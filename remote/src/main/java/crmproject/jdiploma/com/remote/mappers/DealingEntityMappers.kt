package crmproject.jdiploma.com.remote.mappers

import crmproject.jdiploma.com.data.dealing.DealingEntity
import crmproject.jdiploma.com.remote.models.dealing.DealingModel
import crmproject.jdiploma.com.remote.models.user.mappers.UserEntityMapper
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
open class DealingEntityMappers @Inject constructor(
//        private val userEntityMapper: UserEntityMapper,
//        private val contractingPartiesEntityMapper: ContractingPartiesEntityMapper
)