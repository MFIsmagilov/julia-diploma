package crmproject.jdiploma.com.remote.models.section.mappers.impls

import crmproject.jdiploma.com.data.section.model.SectionEntity
import crmproject.jdiploma.com.data.section.repository.SectionRemote
import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.mappers.SectionEntityMapper
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class SectionRemoteImpl @Inject constructor(
        private val crmService: CrmService,
        private val sectionMapper: SectionEntityMapper
) : SectionRemote {
    override fun getSection(): Single<List<SectionEntity>> {
        return crmService.getSections().map {
            it.map {
                sectionMapper.mapFromRemote(it)
            }
        }
    }
}