package crmproject.jdiploma.com.remote.models.task.mappers

import crmproject.jdiploma.com.data.task.model.TaskEntity
import crmproject.jdiploma.com.remote.mappers.EntityMapper
import crmproject.jdiploma.com.remote.models.task.TaskModel
import crmproject.jdiploma.com.remote.models.user.UserModel
import crmproject.jdiploma.com.remote.models.user.mappers.UserEntityMapper
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class TaskEntityMapper @Inject constructor(
        private val userMapper: UserEntityMapper
) : EntityMapper<TaskModel, TaskEntity> {

    //на серваке Task и User имеют связь ManyToMany
    //Из-за этого возраается список idUser
    //мне пока не хочется переделывать поэтому пока так

    override fun mapFromRemote(type: TaskModel): TaskEntity {
        val user = if (type.idUser.isEmpty()) null else type.idUser[0]

        return TaskEntity(
                type.appoinmentDate,
                type.dateCompletion,
                type.comment,
                user?.let { userMapper.mapFromRemote(it) },
                type.status
        )
    }

    override fun mapToRemote(type: TaskEntity): TaskModel {
        val user = type.user?.let { userMapper.mapToRemote(it) }
        val result = if(user == null) listOf<UserModel>() else listOf(user)
        return TaskModel(
                type.appoinmentDate,
                type.dateCompletion,
                type.comment,
                result,
                type.status
        )
    }
}