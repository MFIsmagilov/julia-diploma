package crmproject.jdiploma.com.remote.mappers.impls

import crmproject.jdiploma.com.data.task.model.TaskEntity
import crmproject.jdiploma.com.data.task.repository.TaskRemote
import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.mappers.TaskEntityMapper
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
open class TaskRemoteImpl @Inject constructor(
        private val crmService: CrmService,
        private val taskEntityMapper: TaskEntityMapper
) : TaskRemote {
    override fun getTasks(): Single<List<TaskEntity>> {
        return crmService.getTasks().map {
            it.map { task ->
                taskEntityMapper.mapFromRemote(task)
            }
        }
    }

    override fun createTask(taskEntity: TaskEntity): Single<TaskEntity> {
        return crmService.createTask(
                taskEntityMapper.mapToRemote(taskEntity)
        ).map {
            taskEntityMapper.mapFromRemote(it)
        }
    }
}