package crmproject.jdiploma.com.remote.models.counterparty

/**
 * Created by MaRaT on 25.02.2018.
 */
class ContractPersonsModel(
        val idCP: ContractingPartiesModel,
        val SPN: String,
        val email: String,
        val phone: String,
        val positinFace: String
)