package crmproject.jdiploma.com.remote.mappers

import crmproject.jdiploma.com.data.role.model.RoleEntity
import crmproject.jdiploma.com.remote.models.role.RoleModel
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */

open class RoleEntityMapper @Inject constructor(): EntityMapper<RoleModel, RoleEntity>{
    override fun mapToRemote(type: RoleEntity): RoleModel {
        return RoleModel(
                type.id,
                type.name
        )
    }

    override fun mapFromRemote(type: RoleModel): RoleEntity {
        return RoleEntity(type.id, type.nameRole)
    }

}