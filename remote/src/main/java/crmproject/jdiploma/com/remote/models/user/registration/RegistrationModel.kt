package crmproject.jdiploma.com.remote.models.user.registration

/**
 * Created by MaRaT on 25.02.2018.
 */
class RegistrationModel(
        val nameOrganization: String,
        val siteOrganization: String,
        val username: String,
        val password: String,
        val confirmPassword: String
)