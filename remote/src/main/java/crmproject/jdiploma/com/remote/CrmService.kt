package crmproject.jdiploma.com.remote

import crmproject.jdiploma.com.domain.dealing.Dealing
import crmproject.jdiploma.com.remote.models.counterparty.ContractingPartiesModel
import crmproject.jdiploma.com.remote.models.dealing.DealingModel
import crmproject.jdiploma.com.remote.models.section.SectionModel
import crmproject.jdiploma.com.remote.models.task.TaskModel
import crmproject.jdiploma.com.remote.models.user.UserModel
import crmproject.jdiploma.com.remote.models.user.registration.RegistrationModel
import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path

/**
 * Created by MaRaT on 25.02.2018.
 */
interface CrmService {

    //region user
    @GET("users/")
    fun getUsers(): Single<List<UserModel>>

    @GET("users/{id}/")
    fun getUser(@Path("id") idUser: Int): Single<UserModel>

    @POST("users/")
    fun registrationUser(@Body registrationData: RegistrationModel): Single<UserModel>
    //endregion


    //region task
    @GET("tasks/")
    fun getTasks(): Single<List<TaskModel>>

    @POST("tasks/")
    fun createTask(@Body task: TaskModel): Single<TaskModel>
    //endregion

    //region section
    @GET("sections/")
    fun getSections(): Single<List<SectionModel>>
    //endregion


    //region dealing
    @GET("dealings/")
    fun getDealings(): Single<List<DealingModel>>
    //endregion

    @GET("contracting_parties/")
    fun getContractingParties(): Single<List<ContractingPartiesModel>>
}