package crmproject.jdiploma.com.remote.mappers

import crmproject.jdiploma.com.data.organization.model.OrganizationEntity
import crmproject.jdiploma.com.remote.models.organization.OrganizationModel
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */

open class OrganizationEntityMapper @Inject constructor() : EntityMapper<OrganizationModel, OrganizationEntity> {
    override fun mapToRemote(type: OrganizationEntity): OrganizationModel {
        return OrganizationModel(
                type.id,
                type.name,
                type.site
        )
    }

    override fun mapFromRemote(type: OrganizationModel): OrganizationEntity {
        return OrganizationEntity(type.id, type.nameOrganization, type.siteOrganization)
    }
}