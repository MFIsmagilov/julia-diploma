package crmproject.jdiploma.com.remote.mappers

import crmproject.jdiploma.com.data.section.model.SectionEntity
import crmproject.jdiploma.com.remote.models.section.SectionModel
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
open class SectionEntityMapper @Inject constructor() : EntityMapper<SectionModel, SectionEntity> {
    override fun mapToRemote(type: SectionEntity): SectionModel {
        throw UnsupportedOperationException()
    }

    override fun mapFromRemote(type: SectionModel): SectionEntity {
        return SectionEntity(
                type.nameSec,
                type.linkSection,
                type.isDefault
        )
    }
}