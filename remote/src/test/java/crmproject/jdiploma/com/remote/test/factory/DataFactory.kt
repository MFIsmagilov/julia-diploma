package crmproject.jdiploma.com.remote.test.factory

import java.util.*
import java.util.concurrent.ThreadLocalRandom

/**
 * Created by MaRaT on 25.02.2018.
 */
class DataFactory {

    companion object Factory {

        fun randomUuid(): String {
            return java.util.UUID.randomUUID().toString()
        }

        fun randomInt(): Int {
            return ThreadLocalRandom.current().nextInt(0, 1000 + 1)
        }

        fun randomLong(): Long {
            return randomInt().toLong()
        }

        fun randomBoolean(): Boolean {
            return Math.random() < 0.5
        }

        fun randomDate(): Date {
            //todo: сделать нормально
            return Calendar.getInstance().time
        }

        fun makeStringList(count: Int): List<String> {
            val items: MutableList<String> = mutableListOf()
            repeat(count) {
                items.add(randomUuid())
            }
            return items
        }

    }
}