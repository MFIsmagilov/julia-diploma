package crmproject.jdiploma.com.remote.test.factory

import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.test.factory.DataFactory.Factory.randomBoolean
import crmproject.jdiploma.com.remote.test.factory.DataFactory.Factory.randomDate
import crmproject.jdiploma.com.remote.test.factory.DataFactory.Factory.randomInt
import crmproject.jdiploma.com.remote.test.factory.DataFactory.Factory.randomUuid
import crmproject.jdiploma.com.remote.models.user.DjangoUserModel
import crmproject.jdiploma.com.remote.models.organization.OrganizationModel
import crmproject.jdiploma.com.remote.models.role.RoleModel
import crmproject.jdiploma.com.remote.models.user.UserModel

/**
 * Created by MaRaT on 25.02.2018.
 */
class UserFactory {

    companion object Factory {

//        fun makeUsersResponse(): CrmService.UsersResponse {
//            val usersResponse = CrmService.UsersResponse()
//            usersResponse.users = makeUserModelList(5)
//            return usersResponse
//        }

        fun makeUserModelList(count: Int): List<UserModel> {
            val bufferooEntities = mutableListOf<UserModel>()
            repeat(count) {
                bufferooEntities.add(makeUserModel())
            }
            return bufferooEntities
        }

        fun makeUserModel(): UserModel {
            return UserModel(
                    randomInt(),
                    DjangoUserModel(
                            randomInt(),
                            randomUuid(),
                            randomUuid(),
                            randomUuid(),
                            randomUuid(),
                            randomBoolean(),
                            randomDate()
                    ),
                    RoleModel(
                            randomInt(),
                            randomUuid()
                    ),
                    OrganizationModel(
                            randomInt(),
                            randomUuid(),
                            randomUuid()
                    ),
                    randomUuid()
            )
        }
    }

}