package crmproject.jdiploma.com.remote.mappers

import com.nhaarman.mockito_kotlin.mock
import crmproject.jdiploma.com.remote.models.user.mappers.DjangoUserEntityMapper
import crmproject.jdiploma.com.remote.models.user.mappers.UserEntityMapper
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.JUnit4

/**
 * Created by MaRaT on 25.02.2018.
 */
@RunWith(JUnit4::class)
class UserEntityMapperTest {
    private lateinit var djangoUserMapper: DjangoUserEntityMapper
    private lateinit var roleMapper: RoleEntityMapper
    private lateinit var organizationMapper: OrganizationEntityMapper
    private lateinit var userEntityMapper: UserEntityMapper

    @Before
    fun setUp() {
        djangoUserMapper = mock()
        roleMapper = mock()
        organizationMapper = mock()
        userEntityMapper = UserEntityMapper(djangoUserMapper, roleMapper, organizationMapper)
    }

    @Test
    fun mapFromRemoteMapsData() {
//        val userModel = UserFactory.makeUserModel()
//        val userEntity = userEntityMapper.mapFromRemote(userModel) //wtf?
//
//
//        assertEquals(userModel.id, userEntity.id)
//
//        assertEquals(userModel.idUser.id, userEntity.djangoUser.id)
//        assertEquals(userModel.idUser.email, userEntity.djangoUser.email)
//        assertEquals(userModel.idUser.first_name, userEntity.djangoUser.firstName)
//        assertEquals(userModel.idUser.last_login, userEntity.djangoUser.lastLogin)
//        assertEquals(userModel.idUser.last_name, userEntity.djangoUser.lastName)
//        assertEquals(userModel.idUser.is_superuser, userEntity.djangoUser.isSuperuser)
//
//        assertEquals(userModel.idOrg.id, userEntity.organization.id)
//        assertEquals(userModel.idOrg.nameOrganization, userEntity.organization.name)
//        assertEquals(userModel.idOrg.siteOrganization, userEntity.organization.site)
//
//        assertEquals(userModel.idRole.id, userEntity.role.id)
//        assertEquals(userModel.idRole.nameRole, userEntity.role.name)
//
//        assertEquals(userModel.positionUser, userEntity.positionUser)

    }
}