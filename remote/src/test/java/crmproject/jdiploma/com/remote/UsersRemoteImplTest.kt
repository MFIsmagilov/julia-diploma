package crmproject.jdiploma.com.remote

import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.whenever
import crmproject.jdiploma.com.data.user.models.UserEntity
import crmproject.jdiploma.com.remote.models.user.mappers.UserEntityMapper
import crmproject.jdiploma.com.remote.models.user.mappers.impls.UserRemoteImpl
import crmproject.jdiploma.com.remote.mappers.registration.RegistrationEntityMapper
import crmproject.jdiploma.com.remote.test.factory.UserFactory
import io.reactivex.Single
import io.reactivex.functions.Consumer
import org.junit.Before
import org.junit.Test

/**
 * Created by MaRaT on 25.02.2018.
 */
class UsersRemoteImplTest {

    private lateinit var userEntityMapper: UserEntityMapper
    private lateinit var crmService: CrmService
    private lateinit var usersRemoteImpl: UserRemoteImpl
    private lateinit var registrationEntityMapper: RegistrationEntityMapper

    @Before
    fun setup() {
        userEntityMapper = mock()
        crmService = mock()
        registrationEntityMapper = mock()
        usersRemoteImpl = UserRemoteImpl(crmService, userEntityMapper, registrationEntityMapper)
    }

    @Test
    fun test() {
        CrmServiceFactory.makeCrmService(false).getUsers()
                .subscribe(
                        Consumer {
                            it.forEach {
                                println(it)
                            }
                        }
                )
    }

//    @Test
//    fun getUsersCompletes() {
//        stubBufferooServiceGetBufferoos(Single.just(UserFactory.makeUsersResponse()))
//        val testObserver = usersRemoteImpl.getUsers().test()
//        testObserver.assertComplete()
//    }

    @Test
    fun getUsersReturnsData() {
//        val userResponse = UserFactory.makeUsersResponse()
//        stubBufferooServiceGetBufferoos(Single.just(userResponse))
//        val userEntities = mutableListOf<UserEntity>()
//        userResponse.users.forEach {
//            userEntities.add(userEntityMapper.mapFromRemote(it))
//        }
//        val testObserver = usersRemoteImpl.getUsers().test()
//        testObserver.assertValue(userEntities)
    }

//    private fun stubBufferooServiceGetBufferoos(single: Single<CrmService.UsersResponse>) {
//        whenever(crmService.getUsers())
//                .thenReturn(single)
//    }
}