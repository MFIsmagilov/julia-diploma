package crmproject.jdiploma.com.data.role.model

/**
 * Created by MaRaT on 25.02.2018.
 */
class RoleEntity(
        val id: Int,
        val name: String
)