package crmproject.jdiploma.com.data.counterparty.repository

import crmproject.jdiploma.com.data.counterparty.models.CounterpartyEntity
import io.reactivex.Single

/**
 * Created by MaRaT on 03.06.2018.
 */
interface ContractingPartiesDataStore {

    fun getContractingParties(): Single<List<CounterpartyEntity>>

}