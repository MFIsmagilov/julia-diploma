package crmproject.jdiploma.com.data.user.repository

import crmproject.jdiploma.com.data.user.models.UserEntity
import crmproject.jdiploma.com.data.user.models.RegistrationEntity
import io.reactivex.Single

/**
 * Created by MaRaT on 25.02.2018.
 */
interface UserRemote {

    fun getUsers(): Single<List<UserEntity>>

    fun getUser(id: Int): Single<UserEntity>

    fun registrationUser(obj: RegistrationEntity): Single<UserEntity>

}