package crmproject.jdiploma.com.data.dealing.mapper

import crmproject.jdiploma.com.data.Mapper
import crmproject.jdiploma.com.data.counterparty.mapper.CounterpartyMapper
import crmproject.jdiploma.com.data.dealing.DealingEntity
import crmproject.jdiploma.com.data.user.mapper.UserMapper
import crmproject.jdiploma.com.domain.dealing.Dealing
import javax.inject.Inject

/**
 * Created by MaRaT on 15.03.2018.
 */

class DealingMapper @Inject constructor(
        private val userMapper: UserMapper,
        private val counterpartyMapper: CounterpartyMapper

) : Mapper<DealingEntity, Dealing> {
    override fun mapFromEntity(type: DealingEntity): Dealing {
        return Dealing(
                userMapper.mapFromEntity(type.user),
                counterpartyMapper.mapFromEntity(type.counterparty),
                type.name,
                type.dateBeginning,
                type.dateCompletition,
                type.summa
        )
    }

    override fun mapToEntity(type: Dealing): DealingEntity {
        return DealingEntity(
                userMapper.mapToEntity(type.user),
                counterpartyMapper.mapToEntity(type.counterparty),
                type.name,
                type.dateBeginning,
                type.dateCompletition,
                type.summa
        )
    }

}