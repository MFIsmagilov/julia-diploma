package crmproject.jdiploma.com.data.section.repository

import crmproject.jdiploma.com.data.section.model.SectionEntity
import io.reactivex.Single

/**
 * Created by MaRaT on 05.03.2018.
 */
interface SectionDataStore {

    fun getSection(): Single<List<SectionEntity>>

}