package crmproject.jdiploma.com.data

/**
 * Created by MaRaT on 28.02.2018.
 */
interface Mapper<E, D> {
    fun mapFromEntity(type: E): D

    fun mapToEntity(type: D): E
}