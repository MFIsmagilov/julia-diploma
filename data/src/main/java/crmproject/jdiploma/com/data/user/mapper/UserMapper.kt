package crmproject.jdiploma.com.data.user.mapper

import crmproject.jdiploma.com.data.Mapper
import crmproject.jdiploma.com.data.organization.mapper.OrganizationMapper
import crmproject.jdiploma.com.data.role.mapper.RoleMapper
import crmproject.jdiploma.com.data.user.models.DjangoUserEntity
import crmproject.jdiploma.com.data.user.models.UserEntity
import crmproject.jdiploma.com.domain.user.user.DjangoUser
import crmproject.jdiploma.com.domain.user.user.User
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */

class DjangoUserMapper @Inject constructor() : Mapper<DjangoUserEntity, DjangoUser> {
    override fun mapFromEntity(type: DjangoUserEntity): DjangoUser {
        return DjangoUser(
                type.id,
                type.username,
                type.firstName,
                type.lastName,
                type.email,
                type.isSuperuser,
                type.lastLogin
        )
    }

    override fun mapToEntity(type: DjangoUser): DjangoUserEntity {
        return DjangoUserEntity(
                type.id,
                type.username,
                type.firstName,
                type.lastName,
                type.email,
                type.isSuperuser,
                type.lastLogin
        )
    }
}

class UserMapper @Inject constructor(
        private val djangoUserMapper: DjangoUserMapper,
        private val roleMapper: RoleMapper,
        private val organizationMapper: OrganizationMapper
) : Mapper<UserEntity, User> {
    override fun mapFromEntity(type: UserEntity): User {
        return User(
                type.id,
                djangoUserMapper.mapFromEntity(type.djangoUser),
                roleMapper.mapFromEntity(type.role),
                organizationMapper.mapFromEntity(type.organization),
                type.position
        )
    }

    override fun mapToEntity(type: User): UserEntity {
        return UserEntity(
                type.id,
                djangoUserMapper.mapToEntity(type.djangoUser),
                roleMapper.mapToEntity(type.role),
                organizationMapper.mapToEntity(type.organization),
                type.position
        )
    }
}