package crmproject.jdiploma.com.data.section.mapper

import crmproject.jdiploma.com.data.Mapper
import crmproject.jdiploma.com.data.section.model.SectionEntity
import crmproject.jdiploma.com.domain.section.model.Section
import javax.inject.Inject

/**
 * Created by MaRaT on 04.03.2018.
 */
class SectionMapper @Inject constructor(): Mapper<SectionEntity, Section> {
    override fun mapFromEntity(type: SectionEntity): Section {
        return Section(
                type.name,
                type.linkSection,
                type.isDefault
        )
    }

    override fun mapToEntity(type: Section): SectionEntity {
        return SectionEntity(
                type.nameSec,
                type.linkSection,
                type.isDefault
        )
    }
}