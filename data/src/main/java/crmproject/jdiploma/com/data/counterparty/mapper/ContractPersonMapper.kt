package crmproject.jdiploma.com.data.counterparty.mapper

import crmproject.jdiploma.com.data.Mapper
import crmproject.jdiploma.com.data.counterparty.models.ContractPersonEntity
import crmproject.jdiploma.com.domain.counterparty.model.ContractPerson
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class ContractPersonMapper @Inject constructor(
        private val counterpartyMapper: CounterpartyMapper
) : Mapper<ContractPersonEntity, ContractPerson> {
    override fun mapFromEntity(type: ContractPersonEntity): ContractPerson {
        return ContractPerson(
                type.id,
                counterpartyMapper.mapFromEntity(type.counterparty),
                type.SPN,
                type.email,
                type.phone,
                type.positionFace
        )
    }

    override fun mapToEntity(type: ContractPerson): ContractPersonEntity {
        return ContractPersonEntity(
                type.id,
                counterpartyMapper.mapToEntity(type.contractingParties),
                type.SPN,
                type.email,
                type.phone,
                type.positionFace
        )
    }

}