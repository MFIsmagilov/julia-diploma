package crmproject.jdiploma.com.data.task.repository

import crmproject.jdiploma.com.data.task.model.TaskEntity
import io.reactivex.Single

/**
 * Created by MaRaT on 25.02.2018.
 */
interface TaskRemote {

    fun getTasks(): Single<List<TaskEntity>>

    fun createTask(taskEntity: TaskEntity): Single<TaskEntity>
}