package crmproject.jdiploma.com.data.counterparty.source

import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class ContractingPartiesDataStoreFactory @Inject constructor(
        private val contractingPartiesRemoteDataStore: ContractingPartiesRemoteDataStore
) {
    open fun retrieveRemoteDataStore(): ContractingPartiesRemoteDataStore {
        return contractingPartiesRemoteDataStore
    }
}