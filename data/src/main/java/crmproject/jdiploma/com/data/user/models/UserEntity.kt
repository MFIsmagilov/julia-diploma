package crmproject.jdiploma.com.data.user.models

import crmproject.jdiploma.com.data.organization.model.OrganizationEntity
import crmproject.jdiploma.com.data.role.model.RoleEntity
import java.util.*

/**
 * Created by MaRaT on 25.02.2018.
 */
class DjangoUserEntity(
        val id: Int,
        val username: String,
        val firstName: String,
        val lastName: String,
        val email: String,
        val isSuperuser: Boolean,
        val lastLogin: Date?
)

class UserEntity(
        val id: Int,
        val djangoUser: DjangoUserEntity,
        val role: RoleEntity,
        val organization: OrganizationEntity,
        val position: String
)