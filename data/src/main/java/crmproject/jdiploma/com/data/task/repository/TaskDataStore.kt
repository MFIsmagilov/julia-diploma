package crmproject.jdiploma.com.data.task.repository

import crmproject.jdiploma.com.data.task.model.TaskEntity
import io.reactivex.Single

/**
 * Created by MaRaT on 25.02.2018.
 */
interface TaskDataStore {

    fun getTasks(): Single<List<TaskEntity>>

    fun createTask(task: TaskEntity): Single<TaskEntity>
}