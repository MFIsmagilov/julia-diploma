package crmproject.jdiploma.com.data.section

import crmproject.jdiploma.com.data.section.mapper.SectionMapper
import crmproject.jdiploma.com.data.section.source.SectionDataStoreFactory
import crmproject.jdiploma.com.domain.section.model.Section
import crmproject.jdiploma.com.domain.section.repository.SectionRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 04.03.2018.
 */
class SectionDataRepository @Inject constructor(
        private val factory: SectionDataStoreFactory,
        private val sectionMapper: SectionMapper

) : SectionRepository {
    override fun getSections(): Single<List<Section>> {
        return factory.retrieveRemoteDataStore().getSection()
                .flatMap {
                    Single.just(it)
                }
                .map { list ->
                    list.map { listItem ->
                        sectionMapper.mapFromEntity(listItem)
                    }
                }
    }
}