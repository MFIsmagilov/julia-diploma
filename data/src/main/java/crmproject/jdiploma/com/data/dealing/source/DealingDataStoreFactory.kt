package crmproject.jdiploma.com.data.dealing.source

import javax.inject.Inject

/**
 * Created by MaRaT on 15.03.2018.
 */
open class DealingDataStoreFactory @Inject constructor(
        private val dealingRemoteDataStore: DealingRemoteDataStore
){
    open fun retrieveRemoteDataStore(): DealingRemoteDataStore{
        return dealingRemoteDataStore
    }
}