package crmproject.jdiploma.com.data.organization.model

/**
 * Created by MaRaT on 25.02.2018.
 */
class OrganizationEntity(
        val id: Int,
        val name: String,
        val site: String
)