package crmproject.jdiploma.com.data.user.models

/**
 * Created by MaRaT on 25.02.2018.
 */
class RegistrationEntity(
        val nameOrganization: String,
        val siteOrganization: String,
        val username: String,
        val password: String,
        val confirmPassword: String
)