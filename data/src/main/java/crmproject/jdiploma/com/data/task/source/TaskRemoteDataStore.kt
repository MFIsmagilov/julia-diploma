package crmproject.jdiploma.com.data.task.source

import crmproject.jdiploma.com.data.task.model.TaskEntity
import crmproject.jdiploma.com.data.task.repository.TaskDataStore
import crmproject.jdiploma.com.data.task.repository.TaskRemote
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
open class TaskRemoteDataStore @Inject constructor(
        private val taskRemote: TaskRemote
) : TaskDataStore {

    override fun getTasks(): Single<List<TaskEntity>> {
        return taskRemote.getTasks()
    }

    override fun createTask(task: TaskEntity): Single<TaskEntity> {
        return taskRemote.createTask(
                task
        )
    }
}