package crmproject.jdiploma.com.data.dealing.repository

import crmproject.jdiploma.com.data.dealing.DealingEntity
import io.reactivex.Single

/**
 * Created by MaRaT on 15.03.2018.
 */
interface DealingRemote {
    fun getDealings(): Single<List<DealingEntity>>
}