package crmproject.jdiploma.com.data.user.source

import crmproject.jdiploma.com.data.user.models.UserEntity
import crmproject.jdiploma.com.data.user.models.RegistrationEntity
import crmproject.jdiploma.com.data.user.repository.UserDataStore
import crmproject.jdiploma.com.data.user.repository.UserRemote
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 25.02.2018.
 */
open class UserRemoteDataStore @Inject constructor(
        private val userRemote: UserRemote
) : UserDataStore {
    override fun registerUser(registrationEntity: RegistrationEntity): Single<UserEntity> {
        return userRemote.registrationUser(registrationEntity)
    }

    override fun getUsers(): Single<List<UserEntity>> {
        return userRemote.getUsers()
    }

    override fun getUser(id: Int): Single<UserEntity> {
        return userRemote.getUser(id)
    }
}