package crmproject.jdiploma.com.data.dealing

import crmproject.jdiploma.com.data.counterparty.models.CounterpartyEntity
import crmproject.jdiploma.com.data.user.models.UserEntity
import java.util.*

/**
 * Created by MaRaT on 25.02.2018.
 */
class DealingEntity(
        val user: UserEntity,
        val counterparty: CounterpartyEntity,
        val name: String,
        val dateBeginning: Date,
        val dateCompletition: Date,
        val summa: Int
)