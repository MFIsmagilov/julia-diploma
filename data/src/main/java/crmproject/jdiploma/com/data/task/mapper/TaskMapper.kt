package crmproject.jdiploma.com.data.task.mapper

import crmproject.jdiploma.com.data.Mapper
import crmproject.jdiploma.com.data.task.model.TaskEntity
import crmproject.jdiploma.com.data.user.mapper.UserMapper
import crmproject.jdiploma.com.domain.task.Task
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class TaskMapper @Inject constructor(
        private val userMapper: UserMapper
) : Mapper<TaskEntity, Task> {
    override fun mapFromEntity(type: TaskEntity): Task {
        return Task(
                type.appoinmentDate,
                type.dateCompletion,
                type.comment,
                type.user?.let { userMapper.mapFromEntity(it) },
                type.status
        )
    }

    override fun mapToEntity(type: Task): TaskEntity {
        return TaskEntity(
                type.appoinmentDate,
                type.dateCompletion,
                type.comment,
                type.user?.let { userMapper.mapToEntity(it) },
                type.status
        )
    }
}