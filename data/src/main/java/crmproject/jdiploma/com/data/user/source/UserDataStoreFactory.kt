package crmproject.jdiploma.com.data.user.source

import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
open class UserDataStoreFactory @Inject constructor(
        private val userRemoteDataStore: UserRemoteDataStore
) {
    open fun retrieveRemoteDataStore(): UserRemoteDataStore {
        return userRemoteDataStore
    }
}