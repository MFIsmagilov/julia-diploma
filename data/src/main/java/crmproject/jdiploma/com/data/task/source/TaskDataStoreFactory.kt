package crmproject.jdiploma.com.data.task.source

import crmproject.jdiploma.com.data.user.source.UserRemoteDataStore
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
open class TaskDataStoreFactory @Inject constructor(
        private val taskRemoteDataStore: TaskRemoteDataStore
) {
    open fun retrieveRemoteDataStore(): TaskRemoteDataStore {
        return taskRemoteDataStore
    }
}