package crmproject.jdiploma.com.data.counterparty

import crmproject.jdiploma.com.data.counterparty.mapper.CounterpartyMapper
import crmproject.jdiploma.com.data.counterparty.source.ContractingPartiesDataStoreFactory
import crmproject.jdiploma.com.domain.counterparty.model.Counterparty
import crmproject.jdiploma.com.domain.counterparty.repository.ContractingPartiesRepository
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class ContractingPartiesDataRepository @Inject constructor(
        private val factory: ContractingPartiesDataStoreFactory,
        private val counterpartyMapper: CounterpartyMapper
) : ContractingPartiesRepository {
    override fun getContractingParties(): Single<List<Counterparty>> {
        return factory.retrieveRemoteDataStore().getContractingParties()
                .flatMap {
                    Single.just(it)
                }
                .map {
                    it.map {
                        counterpartyMapper.mapFromEntity(it)
                    }
                }
    }
}