package crmproject.jdiploma.com.data.counterparty.models

/**
 * Created by MaRaT on 03.06.2018.
 */
class CounterpartyEntity(
        val id: Int,
        val name: String,
        val address: String,
        val site: String
)