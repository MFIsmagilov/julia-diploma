package crmproject.jdiploma.com.data.counterparty.mapper

import crmproject.jdiploma.com.data.Mapper
import crmproject.jdiploma.com.data.counterparty.models.CounterpartyEntity
import crmproject.jdiploma.com.domain.counterparty.model.Counterparty
import javax.inject.Inject

/**
 * Created by MaRaT on 15.03.2018.
 */
class CounterpartyMapper @Inject constructor() : Mapper<CounterpartyEntity, Counterparty> {
    override fun mapFromEntity(type: CounterpartyEntity): Counterparty {
        return Counterparty(
                type.id,
                type.name,
                type.address,
                type.site
        )
    }

    override fun mapToEntity(type: Counterparty): CounterpartyEntity {
        return CounterpartyEntity(
                type.id,
                type.name,
                type.address,
                type.site
        )
    }
}