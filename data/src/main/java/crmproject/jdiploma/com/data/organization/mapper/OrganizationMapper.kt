package crmproject.jdiploma.com.data.organization.mapper

import crmproject.jdiploma.com.data.Mapper
import crmproject.jdiploma.com.data.organization.model.OrganizationEntity
import crmproject.jdiploma.com.domain.organization.Organization
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class OrganizationMapper @Inject constructor(): Mapper<OrganizationEntity, Organization> {
    override fun mapFromEntity(type: OrganizationEntity): Organization {
        return Organization(
                type.id,
                type.name,
                type.site
        )
    }

    override fun mapToEntity(type: Organization): OrganizationEntity {
        return OrganizationEntity(
                type.id,
                type.name,
                type.site
        )
    }
}