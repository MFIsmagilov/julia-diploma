package crmproject.jdiploma.com.data.counterparty.source

import crmproject.jdiploma.com.data.counterparty.models.CounterpartyEntity
import crmproject.jdiploma.com.data.counterparty.repository.ContractingPartiesDataStore
import crmproject.jdiploma.com.data.counterparty.repository.ContractingPartiesRemote
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class ContractingPartiesRemoteDataStore @Inject constructor(
        private val contractingPartiesRemote: ContractingPartiesRemote
) : ContractingPartiesDataStore {
    override fun getContractingParties(): Single<List<CounterpartyEntity>> {
        return contractingPartiesRemote.getContractingParties()
    }
}