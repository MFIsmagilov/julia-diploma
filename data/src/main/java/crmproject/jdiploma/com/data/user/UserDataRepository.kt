package crmproject.jdiploma.com.data.user

import crmproject.jdiploma.com.data.user.mapper.UserMapper
import crmproject.jdiploma.com.data.user.source.UserDataStoreFactory
import crmproject.jdiploma.com.domain.user.model.Registration
import crmproject.jdiploma.com.domain.user.repository.UserRepository
import crmproject.jdiploma.com.domain.user.user.User
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class UserDataRepository @Inject constructor(
        private val factory: UserDataStoreFactory,
        private val userMapper: UserMapper

) : UserRepository {
    override fun getUsers(): Single<List<User>> {
        return factory.retrieveRemoteDataStore().getUsers()
                .flatMap {
                    //todo: здесь можно сделать сохранение в кэш
                    Single.just(it)
                }
                .map { list ->
                    list.map { listItem ->
                        userMapper.mapFromEntity(listItem)
                    }
                }
    }

    override fun getUser(id: Int): Single<User> {
        return factory.retrieveRemoteDataStore().getUser(id).map {
            userMapper.mapFromEntity(it)
        }
    }

    override fun registerUser(registerData: Registration): Single<User> {
        TODO("not implemented class UserDataRepository") //To change body of created functions use File | Settings | File Templates.
    }
}