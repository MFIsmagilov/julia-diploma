package crmproject.jdiploma.com.data.role.mapper

import crmproject.jdiploma.com.data.Mapper
import crmproject.jdiploma.com.data.role.model.RoleEntity
import crmproject.jdiploma.com.domain.role.Role
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class RoleMapper @Inject constructor(): Mapper<RoleEntity, Role> {
    override fun mapFromEntity(type: RoleEntity): Role {
        return Role(
                type.id,
                type.name
        )
    }

    override fun mapToEntity(type: Role): RoleEntity {
        return RoleEntity(
                type.id,
                type.name
        )
    }
}