package crmproject.jdiploma.com.data.task.model

import crmproject.jdiploma.com.data.user.models.UserEntity
import java.util.*

/**
 * Created by MaRaT on 25.02.2018.
 */
class TaskEntity(
        val appoinmentDate: Date,
        val dateCompletion: Date,
        val comment: String,
        val user: UserEntity?,
        val status: String
)