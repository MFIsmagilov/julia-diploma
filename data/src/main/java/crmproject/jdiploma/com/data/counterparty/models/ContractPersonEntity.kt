package crmproject.jdiploma.com.data.counterparty.models


/**
 * Created by MaRaT on 03.06.2018.
 */
class ContractPersonEntity(
        val id: Int,
        val counterparty: CounterpartyEntity,
        val SPN: String,
        val email: String,
        val phone: String,
        val positionFace: String
)