package crmproject.jdiploma.com.data.task

import crmproject.jdiploma.com.data.task.mapper.TaskMapper
import crmproject.jdiploma.com.data.task.source.TaskDataStoreFactory
import crmproject.jdiploma.com.data.user.mapper.UserMapper
import crmproject.jdiploma.com.data.user.source.UserDataStoreFactory
import crmproject.jdiploma.com.domain.task.Task
import crmproject.jdiploma.com.domain.task.TaskRepository
import crmproject.jdiploma.com.domain.user.model.Registration
import crmproject.jdiploma.com.domain.user.repository.UserRepository
import crmproject.jdiploma.com.domain.user.user.User
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class TaskDataRepository @Inject constructor(
        private val factory: TaskDataStoreFactory,
        private val taskMapper: TaskMapper
) : TaskRepository {
    override fun getTasks(): Single<List<Task>> {
        return factory.retrieveRemoteDataStore().getTasks()
                .flatMap {
                    Single.just(it)
                }
                .map { list ->
                    list.map { listItem ->
                        taskMapper.mapFromEntity(listItem)
                    }
                }
    }

    override fun createTask(task: Task): Single<Task> {
        return factory.retrieveRemoteDataStore().createTask(taskMapper.mapToEntity(task))
                .map {
                    taskMapper.mapFromEntity(it)
                }
    }
}