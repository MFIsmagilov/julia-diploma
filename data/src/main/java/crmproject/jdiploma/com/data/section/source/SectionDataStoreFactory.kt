package crmproject.jdiploma.com.data.section.source

import crmproject.jdiploma.com.data.section.repository.SectionDataStore
import crmproject.jdiploma.com.data.user.source.UserRemoteDataStore
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
open class SectionDataStoreFactory @Inject constructor(
        private val sectionDataStore: SectionRemoteDataStore
) {
    open fun retrieveRemoteDataStore(): SectionDataStore {
        return sectionDataStore
    }
}