package crmproject.jdiploma.com.data.section.source

import crmproject.jdiploma.com.data.section.model.SectionEntity
import crmproject.jdiploma.com.data.section.repository.SectionDataStore
import crmproject.jdiploma.com.data.section.repository.SectionRemote
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class SectionRemoteDataStore @Inject constructor(
        private val sectionRemote: SectionRemote
): SectionDataStore {
    override fun getSection(): Single<List<SectionEntity>> {
        return sectionRemote.getSection()
    }
}