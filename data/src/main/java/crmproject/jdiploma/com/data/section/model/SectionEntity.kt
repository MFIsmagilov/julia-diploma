package crmproject.jdiploma.com.data.section.model

/**
 * Created by MaRaT on 25.02.2018.
 */
class SectionEntity(
        val name: String,
        val linkSection: String,
        val isDefault: Boolean
)