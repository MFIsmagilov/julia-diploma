package crmproject.jdiploma.com.data.dealing.source

import crmproject.jdiploma.com.data.dealing.DealingEntity
import crmproject.jdiploma.com.data.dealing.repository.DealingDataStore
import crmproject.jdiploma.com.data.dealing.repository.DealingRemote
import io.reactivex.Single
import javax.inject.Inject

/**
 * Created by MaRaT on 15.03.2018.
 */
open class DealingRemoteDataStore @Inject constructor(
        private val dealingRemote: DealingRemote
): DealingDataStore{
    override fun getDealings(): Single<List<DealingEntity>> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}