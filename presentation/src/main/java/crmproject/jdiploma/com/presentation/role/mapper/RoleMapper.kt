package crmproject.jdiploma.com.presentation.role.mapper

import crmproject.jdiploma.com.domain.role.Role
import crmproject.jdiploma.com.presentation.Mapper
import crmproject.jdiploma.com.presentation.role.model.RoleView
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class RoleMapper @Inject constructor(): Mapper<RoleView, Role> {
    override fun mapToView(type: Role): RoleView {
        return RoleView(
                type.id,
                type.name
        )
    }

    override fun mapFromView(type: RoleView): Role {
        return Role(
                type.id,
                type.name
        )
    }
}