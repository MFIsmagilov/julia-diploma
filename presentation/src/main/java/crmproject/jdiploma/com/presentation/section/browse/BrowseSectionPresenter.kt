package crmproject.jdiploma.com.presentation.section.browse

import crmproject.jdiploma.com.domain.section.interactor.SectionSingleUseCase
import crmproject.jdiploma.com.domain.section.model.Section
import crmproject.jdiploma.com.domain.user.interactor.UserSingleUseCase
import crmproject.jdiploma.com.domain.user.user.User
import crmproject.jdiploma.com.presentation.section.mapper.SectionMapper
import crmproject.jdiploma.com.presentation.user.mapper.UserMapper
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class BrowseSectionPresenter @Inject constructor(
        val browseView: BrowseSectionContract.View,
        val sectionSingleUseCase: SectionSingleUseCase<List<Section>, Void>,
        val sectionMapper: SectionMapper,

        val userSingleUseCase: UserSingleUseCase<User, Int>,
        val userMapper: UserMapper

) : BrowseSectionContract.Presenter {

    override fun getUser(id: Int) {
        userSingleUseCase.execute(object: DisposableSingleObserver<User>(){
            override fun onError(e: Throwable) {
                handleGetUserError(e)
            }

            override fun onSuccess(t: User) {
                browseView.showUser(userMapper.mapToView(t))
            }

        }, id)
    }

    init {
        browseView.setPresenter(this)
    }

    override fun start() {
        browseView.showProgress()
    }

    override fun stop() {
        sectionSingleUseCase.dispose()
    }

    internal fun handleGetUserSuccess(sections: List<Section>) {
        browseView.showSection(
                sections.map {
                    sectionMapper.mapToView(it)
                }
        )
    }

    internal fun handleGetUserError(exception: Throwable) {
        browseView.showError(exception)
    }

    override fun getSections() {
        start()
        sectionSingleUseCase.execute(SectionSubscriber())
    }

    inner class SectionSubscriber : DisposableSingleObserver<List<Section>>() {
        override fun onSuccess(t: List<Section>) {
            handleGetUserSuccess(t)
        }

        override fun onError(e: Throwable) {
            handleGetUserError(e)
        }
    }
}