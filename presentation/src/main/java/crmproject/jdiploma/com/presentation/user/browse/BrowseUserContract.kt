package crmproject.jdiploma.com.presentation.user.browse

import android.arch.lifecycle.LifecycleObserver
import crmproject.jdiploma.com.domain.user.user.User
import crmproject.jdiploma.com.presentation.BasePresenter
import crmproject.jdiploma.com.presentation.BaseView
import crmproject.jdiploma.com.presentation.user.model.UserView

/**
 * Created by MaRaT on 28.02.2018.
 */
interface BrowseUserContract {
    interface View : BaseView<Presenter> {

        fun showProgress()

        fun hideProgress()

        fun showUserData(user: UserView)

        fun showError(exception: Throwable)

        fun showUsers(users: List<UserView>)

    }

    interface Presenter : BasePresenter {

        fun showUserById(id: Int)

        fun showUsers()
    }
}