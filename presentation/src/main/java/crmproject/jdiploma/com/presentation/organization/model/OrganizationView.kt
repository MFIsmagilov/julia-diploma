package crmproject.jdiploma.com.presentation.organization.model

/**
 * Created by MaRaT on 28.02.2018.
 */
class OrganizationView(
        val id: Int,
        val name: String,
        val site: String
)