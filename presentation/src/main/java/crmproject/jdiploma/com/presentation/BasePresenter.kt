package crmproject.jdiploma.com.presentation

/**
 * Created by MaRaT on 28.02.2018.
 */
interface BasePresenter {

    fun start()

    fun stop()

}