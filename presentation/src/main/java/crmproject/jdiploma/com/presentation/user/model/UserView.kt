package crmproject.jdiploma.com.presentation.user.model

import crmproject.jdiploma.com.presentation.organization.model.OrganizationView
import crmproject.jdiploma.com.presentation.role.model.RoleView
import java.util.*

/**
 * Created by MaRaT on 28.02.2018.
 */
class DjangoUserView(
        val id: Int,
        val username: String,
        val firstName: String,
        val lastName: String,
        val email: String,
        val isSuperuser: Boolean,
        val lastLogin: Date?
)

class UserView(
        val id: Int,
        val djangoUser: DjangoUserView,
        val role: RoleView,
        val organization: OrganizationView,
        val position: String
)