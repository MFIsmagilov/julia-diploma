package crmproject.jdiploma.com.presentation.task

import crmproject.jdiploma.com.presentation.BasePresenter
import crmproject.jdiploma.com.presentation.BaseView
import crmproject.jdiploma.com.presentation.user.model.UserView
import io.reactivex.Single

/**
 * Created by MaRaT on 07.03.2018.
 */
interface BrowseTaskContract {

    interface View : BaseView<Presenter> {

        fun showProgress()

        fun hideProgress()

        fun showTasksData(taskView: List<TaskView>)

        fun showError(exception: Throwable)

        fun failedLoaded()

        fun succeedLoaded()

        fun taskCreatedSucceed(task: TaskView)
    }

    interface Presenter : BasePresenter {

        fun showTasks()

        fun updateTasks()

        fun createTask(task: TaskView)
    }

}