package crmproject.jdiploma.com.presentation.task

import crmproject.jdiploma.com.presentation.user.model.UserView
import java.util.*

/**
 * Created by MaRaT on 07.03.2018.
 */
class TaskView(
        val appoinmentDate: Date,
        val dateCompletion: Date,
        val comment: String,
        val user: UserView?,
        val status: String
)