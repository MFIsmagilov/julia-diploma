package crmproject.jdiploma.com.presentation.counterparty.mapper

import crmproject.jdiploma.com.domain.counterparty.model.Counterparty
import crmproject.jdiploma.com.presentation.Mapper
import crmproject.jdiploma.com.presentation.counterparty.model.CounterpartyView
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class CounterpartyMapper @Inject constructor() : Mapper<CounterpartyView, Counterparty> {
    override fun mapToView(type: Counterparty): CounterpartyView {
        return CounterpartyView(
                type.id,
                type.name,
                type.address,
                type.site
        )
    }

    override fun mapFromView(type: CounterpartyView): Counterparty {
        return Counterparty(
                type.id,
                type.name,
                type.address,
                type.site
        )
    }
}