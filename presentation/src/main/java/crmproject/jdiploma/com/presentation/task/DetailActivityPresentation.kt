package crmproject.jdiploma.com.presentation.task

import crmproject.jdiploma.com.domain.task.Task
import crmproject.jdiploma.com.domain.task.interactor.TaskUseCase
import crmproject.jdiploma.com.domain.user.interactor.UserSingleUseCase
import crmproject.jdiploma.com.domain.user.user.User
import crmproject.jdiploma.com.presentation.BasePresenter
import crmproject.jdiploma.com.presentation.BaseView
import crmproject.jdiploma.com.presentation.user.mapper.UserMapper
import crmproject.jdiploma.com.presentation.user.model.UserView
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by MaRaT on 12.03.2018.
 */
interface DetailActivityContract {


    interface View : BaseView<Presenter> {

        fun showProgress()

        fun hideProgress()

        fun loadUsersSucced(users: List<UserView>)

        fun loadUsersFailure(ex: Throwable)

        fun createTaskSucced(task: TaskView)

        fun createTaskFailure(ex: Throwable)

    }

    interface Presenter : BasePresenter {

        fun showUsers()

        fun createTask(taskView: TaskView)

    }

}

class DetailTaskPresenter @Inject constructor(
        val browseView: DetailActivityContract.View,
        val taskUseCase: TaskUseCase<Task, Int>,
        val userUseCase: UserSingleUseCase<User, Int>,
        val taskMapper: TaskMapper,
        val userMapper: UserMapper
) : DetailActivityContract.Presenter {
    override fun createTask(taskView: TaskView) {
        browseView.showProgress()
        val task = taskMapper.mapFromView(taskView)
        taskUseCase.createTask(object : DisposableSingleObserver<Task>() {
            override fun onSuccess(t: Task) {
                browseView.createTaskSucced(taskMapper.mapToView(t))
            }

            override fun onError(e: Throwable) {
                browseView.createTaskFailure(e)
                browseView.hideProgress()
            }

        }, task)
    }

    override fun showUsers() {
        browseView.showProgress()
        userUseCase.loadUsers(object : DisposableSingleObserver<List<User>>() {
            override fun onSuccess(t: List<User>) {
                val usersView = t.map {
                    userMapper.mapToView(it)
                }
                browseView.loadUsersSucced(usersView)
                browseView.hideProgress()
            }

            override fun onError(e: Throwable) {
                browseView.hideProgress()
                browseView.loadUsersFailure(e)
            }
        })
    }


    override fun start() {
    }

    override fun stop() {
        taskUseCase.dispose()
        userUseCase.dispose()
    }

    init {
        browseView.setPresenter(this)
    }
}