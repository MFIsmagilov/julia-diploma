package crmproject.jdiploma.com.presentation.section.mapper

import crmproject.jdiploma.com.domain.section.model.Section
import crmproject.jdiploma.com.presentation.Mapper
import crmproject.jdiploma.com.presentation.section.model.SectionView
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class SectionMapper @Inject constructor(
) : Mapper<SectionView, Section> {
    override fun mapToView(type: Section): SectionView {
        return SectionView(
                type.nameSec,
                type.linkSection,
                type.isDefault
        )
    }

    override fun mapFromView(type: SectionView): Section {
        return Section(
                type.nameSec,
                type.linkSection,
                type.isDefault
        )
    }
}