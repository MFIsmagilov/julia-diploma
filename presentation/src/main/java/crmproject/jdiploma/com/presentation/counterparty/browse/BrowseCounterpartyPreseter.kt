package crmproject.jdiploma.com.presentation.counterparty.browse

import crmproject.jdiploma.com.domain.counterparty.interactor.CouterpartySingleUseCase
import crmproject.jdiploma.com.domain.counterparty.model.Counterparty
import crmproject.jdiploma.com.presentation.counterparty.mapper.CounterpartyMapper
import crmproject.jdiploma.com.presentation.counterparty.model.CounterpartyView
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */

class BrowseCounterpartyPreseter @Inject constructor(
        val browseView: BrowseCounterparty.View,
        val counterpartSingleUseCase: CouterpartySingleUseCase<Counterparty, Void>,
        val mapper: CounterpartyMapper
) : BrowseCounterparty.Presenter {

    init {
        browseView.setPresenter(this)
    }

    override fun start() {
        browseView.showProgress()
    }

    override fun stop() {
        counterpartSingleUseCase.dispose()
    }

    override fun showContractingParties() {
        start()
        counterpartSingleUseCase.loadContractingparties(object : DisposableSingleObserver<List<Counterparty>>() {
            override fun onError(e: Throwable) {
                browseView.showError(e)
            }

            override fun onSuccess(t: List<Counterparty>) {
                browseView.showContractingParties(
                        t.map {
                            mapper.mapToView(it)
                        }
                )
            }
        })
    }

}