package crmproject.jdiploma.com.presentation.user.mapper

import crmproject.jdiploma.com.domain.user.user.DjangoUser
import crmproject.jdiploma.com.domain.user.user.User
import crmproject.jdiploma.com.presentation.Mapper
import crmproject.jdiploma.com.presentation.organization.mapper.OrganizationMapper
import crmproject.jdiploma.com.presentation.role.mapper.RoleMapper
import crmproject.jdiploma.com.presentation.user.model.DjangoUserView
import crmproject.jdiploma.com.presentation.user.model.UserView
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class DjangoUserMapper @Inject constructor() : Mapper<DjangoUserView, DjangoUser> {

    override fun mapToView(type: DjangoUser): DjangoUserView {
        return DjangoUserView(
                type.id,
                type.username,
                type.firstName,
                type.lastName,
                type.email,
                type.isSuperuser,
                type.lastLogin
        )
    }

    override fun mapFromView(type: DjangoUserView): DjangoUser {
        return DjangoUser(
                type.id,
                type.username,
                type.firstName,
                type.lastName,
                type.email,
                type.isSuperuser,
                type.lastLogin
        )
    }
}

class UserMapper @Inject constructor(
        private val djangoUserMapper: DjangoUserMapper,
        private val roleMapper: RoleMapper,
        private val organizationMapper: OrganizationMapper
) : Mapper<UserView, User> {

    override fun mapToView(type: User): UserView {
        return UserView(
                type.id,
                djangoUserMapper.mapToView(type.djangoUser),
                roleMapper.mapToView(type.role),
                organizationMapper.mapToView(type.organization),
                type.position
        )
    }

    override fun mapFromView(type: UserView): User {
        return User(
                type.id,
                djangoUserMapper.mapFromView(type.djangoUser),
                roleMapper.mapFromView(type.role),
                organizationMapper.mapFromView(type.organization),
                type.position
        )
    }

}