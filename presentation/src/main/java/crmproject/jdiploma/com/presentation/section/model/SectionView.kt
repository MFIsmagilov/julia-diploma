package crmproject.jdiploma.com.presentation.section.model

/**
 * Created by MaRaT on 05.03.2018.
 */
class SectionView(
        val nameSec: String,
        val linkSection: String,
        val isDefault: Boolean
)