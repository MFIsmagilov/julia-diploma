package crmproject.jdiploma.com.presentation.counterparty.model

/**
 * Created by MaRaT on 03.06.2018.
 */
class CounterpartyView(
        val id: Int,
        val name: String,
        val address: String,
        val site: String
)