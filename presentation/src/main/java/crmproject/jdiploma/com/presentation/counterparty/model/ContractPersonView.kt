package crmproject.jdiploma.com.presentation.counterparty.model

/**
 * Created by MaRaT on 03.06.2018.
 */
class ContractPersonView(
        val id: Int,
        val counterparty: CounterpartyView,
        val SPN: String,
        val email: String,
        val phone: String,
        val positionFace: String
)