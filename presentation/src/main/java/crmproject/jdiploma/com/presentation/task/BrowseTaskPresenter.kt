package crmproject.jdiploma.com.presentation.task

import crmproject.jdiploma.com.domain.task.Task
import crmproject.jdiploma.com.domain.task.interactor.TaskUseCase
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by MaRaT on 07.03.2018.
 */
class BrowseTaskPresenter @Inject constructor(
        val browseView: BrowseTaskContract.View,
        val taskUseCase: TaskUseCase<Task, Int>,
        val taskMapper: TaskMapper
) : BrowseTaskContract.Presenter {

    init {
        browseView.setPresenter(this)
    }

    override fun start() {
        browseView.showProgress()
    }

    override fun stop() {
        taskUseCase.dispose()
    }

    override fun showTasks() {
        browseView.showProgress()
        taskUseCase.loadTask(object : DisposableSingleObserver<List<Task>>() {
            override fun onError(e: Throwable) {
                browseView.showError(e)
                browseView.failedLoaded()
                browseView.hideProgress()
            }

            override fun onSuccess(t: List<Task>) {
                browseView.succeedLoaded()
                browseView.hideProgress()
                browseView.showTasksData(
                        t.map {
                            taskMapper.mapToView(it)
                        }
                )
            }
        })
    }

    override fun updateTasks() {
        showTasks() //Fixme showTasks from Cache , update from Remote
    }

    override fun createTask(task: TaskView) {
        taskUseCase.createTask(object : DisposableSingleObserver<Task>() {
            override fun onSuccess(t: Task) {
                browseView.taskCreatedSucceed(
                        taskMapper.mapToView(t)
                )
            }

            override fun onError(e: Throwable) {
                browseView.showError(e)
            }

        }, taskMapper.mapFromView(task))
    }
}