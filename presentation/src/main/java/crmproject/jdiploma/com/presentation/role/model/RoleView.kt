package crmproject.jdiploma.com.presentation.role.model

/**
 * Created by MaRaT on 28.02.2018.
 */
class RoleView(
        val id: Int,
        val name: String
)