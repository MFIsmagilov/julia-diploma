package crmproject.jdiploma.com.presentation.user.browse

import crmproject.jdiploma.com.domain.user.interactor.UserSingleUseCase
import crmproject.jdiploma.com.domain.user.user.User
import crmproject.jdiploma.com.presentation.user.mapper.UserMapper
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class BrowseUserPresenter @Inject constructor(val browseView: BrowseUserContract.View,
                                              val userSingleCase: UserSingleUseCase<User, Int>,
                                              val userMapper: UserMapper) :
        BrowseUserContract.Presenter {

    override fun showUsers() {
        start()
        userSingleCase.loadUsers(object : DisposableSingleObserver<List<User>>() {
            override fun onError(e: Throwable) {
                handleGetUserError(e)
            }

            override fun onSuccess(t: List<User>) {
                browseView.showUsers(
                        t.map {
                            userMapper.mapToView(it)
                        }
                )
            }
        })
    }

    init {
        browseView.setPresenter(this)
    }

    override fun showUserById(id: Int) {
        start()
        userSingleCase.execute(UserSubscriber(), id)
    }

    override fun start() {
        browseView.showProgress()
    }

    override fun stop() {
        userSingleCase.dispose()
    }

    internal fun handleGetUserSuccess(user: User) {
        browseView.showUserData(userMapper.mapToView(user))
    }

    internal fun handleGetUserError(exception: Throwable) {
        browseView.showError(exception)
    }

    inner class UserSubscriber : DisposableSingleObserver<User>() {

        override fun onSuccess(t: User) {
            handleGetUserSuccess(t)
        }

        override fun onError(exception: Throwable) {
            handleGetUserError(exception)
        }
    }

}