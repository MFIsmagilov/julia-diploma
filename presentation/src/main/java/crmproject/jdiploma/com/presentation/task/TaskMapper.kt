package crmproject.jdiploma.com.presentation.task

import crmproject.jdiploma.com.domain.task.Task
import crmproject.jdiploma.com.presentation.Mapper
import crmproject.jdiploma.com.presentation.user.mapper.UserMapper
import javax.inject.Inject

/**
 * Created by MaRaT on 07.03.2018.
 */
class TaskMapper @Inject constructor(
        private val userMapper: UserMapper
) : Mapper<TaskView, Task> {
    override fun mapToView(type: Task): TaskView {
        return TaskView(
                type.appoinmentDate,
                type.dateCompletion,
                type.comment,
                type.user?.let { userMapper.mapToView(it) },
                type.status
        )
    }

    override fun mapFromView(type: TaskView): Task {
        return Task(
                type.appoinmentDate,
                type.dateCompletion,
                type.comment,
                type.user?.let { userMapper.mapFromView(it) },
                type.status
        )
    }
}