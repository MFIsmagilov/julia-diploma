package crmproject.jdiploma.com.presentation.task

import crmproject.jdiploma.com.domain.task.Task
import crmproject.jdiploma.com.domain.task.interactor.TaskUseCase
import crmproject.jdiploma.com.presentation.BasePresenter
import crmproject.jdiploma.com.presentation.BaseView
import io.reactivex.observers.DisposableSingleObserver
import javax.inject.Inject

/**
 * Created by MaRaT on 12.03.2018.
 */
interface AddTaskActivityContract {

    interface View : BaseView<Presenter> {

        fun showProgress()

        fun hideProgress()

        fun createTaskSucced(task: TaskView)

        fun createTaskFailure(ex: Throwable)

    }

    interface Presenter : BasePresenter {

        fun createTask(taskView: TaskView)

    }

}

class AddTaskPresenter @Inject constructor(
        val browseView: AddTaskActivityContract.View,
        val taskUseCase: TaskUseCase<Task, Int>,
//        val userUseCase: UserSingleUseCase<User, Int>,
        val taskMapper: TaskMapper
//        val userMapper: UserMapper
) : AddTaskActivityContract.Presenter {
    override fun createTask(taskView: TaskView) {
        browseView.showProgress()
        val task = taskMapper.mapFromView(taskView)
        taskUseCase.createTask(object : DisposableSingleObserver<Task>() {
            override fun onSuccess(t: Task) {
                browseView.createTaskSucced(taskMapper.mapToView(t))
            }

            override fun onError(e: Throwable) {
                browseView.createTaskFailure(e)
                browseView.hideProgress()
            }

        }, task)
    }

    override fun start() {
    }

    override fun stop() {
        taskUseCase.dispose()
    }

    init {
        browseView.setPresenter(this)
    }
}