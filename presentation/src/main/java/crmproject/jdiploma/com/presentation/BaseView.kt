package crmproject.jdiploma.com.presentation

/**
 * Created by MaRaT on 28.02.2018.
 */
interface BaseView<in T : BasePresenter> {

    fun setPresenter(presenter: T)

}