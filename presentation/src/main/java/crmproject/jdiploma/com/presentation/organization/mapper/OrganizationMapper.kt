package crmproject.jdiploma.com.presentation.organization.mapper

import crmproject.jdiploma.com.domain.organization.Organization
import crmproject.jdiploma.com.presentation.Mapper
import crmproject.jdiploma.com.presentation.organization.model.OrganizationView
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class OrganizationMapper @Inject constructor() : Mapper<OrganizationView, Organization> {
    override fun mapToView(type: Organization): OrganizationView {
        return OrganizationView(
                type.id,
                type.name,
                type.site
        )
    }

    override fun mapFromView(type: OrganizationView): Organization {
        return Organization(
                type.id,
                type.name,
                type.site
        )
    }
}