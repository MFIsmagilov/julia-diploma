package crmproject.jdiploma.com.presentation.section.browse

import crmproject.jdiploma.com.presentation.BasePresenter
import crmproject.jdiploma.com.presentation.BaseView
import crmproject.jdiploma.com.presentation.section.model.SectionView
import crmproject.jdiploma.com.presentation.user.model.UserView

/**
 * Created by MaRaT on 05.03.2018.
 */
interface BrowseSectionContract {

    interface View : BaseView<Presenter> {

        fun showProgress()

        fun hideProgress()

        fun showError(exception: Throwable)

        fun showSection(section: List<SectionView>)

        fun showUser(user: UserView)

    }

    interface Presenter : BasePresenter {
        fun getSections()

        fun getUser(id: Int)
    }
}