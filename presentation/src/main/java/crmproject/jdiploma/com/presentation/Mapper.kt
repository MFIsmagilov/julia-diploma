package crmproject.jdiploma.com.presentation

/**
 * Created by MaRaT on 28.02.2018.
 */
interface Mapper<V, D> {

    fun mapToView(type: D): V

    fun mapFromView(type: V): D

}