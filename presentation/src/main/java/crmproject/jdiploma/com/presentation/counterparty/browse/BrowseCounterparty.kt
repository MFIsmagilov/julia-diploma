package crmproject.jdiploma.com.presentation.counterparty.browse

import crmproject.jdiploma.com.presentation.BasePresenter
import crmproject.jdiploma.com.presentation.BaseView
import crmproject.jdiploma.com.presentation.counterparty.model.CounterpartyView

/**
 * Created by MaRaT on 03.06.2018.
 */
interface BrowseCounterparty {
    interface View : BaseView<Presenter> {

        fun showProgress()

        fun hideProgress()

        fun showError(exception: Throwable)

        fun showContractingParties(contractingParties: List<CounterpartyView>)
    }

    interface Presenter : BasePresenter {

        fun showContractingParties()
    }
}