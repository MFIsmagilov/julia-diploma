package crmproject.jdiploma.com.crmproject.contractingparties.model

/**
 * Created by MaRaT on 03.06.2018.
 */
class CounterpartyViewModel(
        val id: Int,
        val name: String,
        val address: String,
        val site: String
)