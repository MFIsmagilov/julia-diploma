package crmproject.jdiploma.com.crmproject

import android.app.Application
import android.content.Context
import crmproject.jdiploma.com.crmproject.BuildConfig
import crmproject.jdiploma.com.crmproject.UiThread
import crmproject.jdiploma.com.crmproject.injection.scopes.PerApplication
import crmproject.jdiploma.com.data.counterparty.ContractingPartiesDataRepository
import crmproject.jdiploma.com.data.counterparty.repository.ContractingPartiesRemote
import crmproject.jdiploma.com.data.counterparty.source.ContractingPartiesDataStoreFactory
import crmproject.jdiploma.com.data.task.mapper.TaskMapper
import crmproject.jdiploma.com.data.executor.JobExecutor
import crmproject.jdiploma.com.data.section.SectionDataRepository
import crmproject.jdiploma.com.data.section.mapper.SectionMapper
import crmproject.jdiploma.com.data.section.repository.SectionRemote
import crmproject.jdiploma.com.data.section.source.SectionDataStoreFactory
import crmproject.jdiploma.com.data.task.TaskDataRepository
import crmproject.jdiploma.com.data.task.repository.TaskRemote
import crmproject.jdiploma.com.data.task.source.TaskDataStoreFactory
import crmproject.jdiploma.com.data.user.UserDataRepository
import crmproject.jdiploma.com.data.user.mapper.UserMapper
import crmproject.jdiploma.com.data.user.repository.UserRemote
import crmproject.jdiploma.com.data.user.source.UserDataStoreFactory
import crmproject.jdiploma.com.domain.counterparty.repository.ContractingPartiesRepository
import crmproject.jdiploma.com.domain.executor.PostExecutionThread
import crmproject.jdiploma.com.domain.executor.ThreadExecutor
import crmproject.jdiploma.com.domain.section.repository.SectionRepository
import crmproject.jdiploma.com.domain.task.TaskRepository
import crmproject.jdiploma.com.domain.user.repository.UserRepository
import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.CrmServiceFactory
import crmproject.jdiploma.com.remote.mappers.CounterpartyMapper
import crmproject.jdiploma.com.remote.mappers.SectionEntityMapper
import crmproject.jdiploma.com.remote.mappers.TaskEntityMapper
import crmproject.jdiploma.com.remote.mappers.impls.TaskRemoteImpl
import crmproject.jdiploma.com.remote.models.user.mappers.UserEntityMapper
import crmproject.jdiploma.com.remote.models.user.mappers.impls.UserRemoteImpl
import crmproject.jdiploma.com.remote.mappers.registration.RegistrationEntityMapper
import crmproject.jdiploma.com.remote.models.counterparty.mappers.CounterpartyEntityMapper
import crmproject.jdiploma.com.remote.models.counterparty.mappers.impl.ContractingPartiesRemoteImpl
import crmproject.jdiploma.com.remote.models.section.mappers.impls.SectionRemoteImpl
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 01.03.2018.
 */
@Module
open class ApplicationModule {

    @Provides
    @PerApplication
    fun provideContext(application: Application): Context {
        return application
    }


    @Provides
    @PerApplication
    internal fun provideSectionRepository(
            factory: SectionDataStoreFactory,
            mapper: SectionMapper): SectionRepository {
        return SectionDataRepository(factory, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideSectionRemote(
            service: CrmService,
            factory: SectionEntityMapper): SectionRemote {
        return SectionRemoteImpl(service, factory)
    }

    @Provides
    @PerApplication
    internal fun provideTaskRepository(
            factory: TaskDataStoreFactory,
            mapper: TaskMapper): TaskRepository {
        return TaskDataRepository(factory, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideTaskRemote(
            service: CrmService,
            factory: TaskEntityMapper): TaskRemote {
        return TaskRemoteImpl(service, factory)
    }

    @Provides
    @PerApplication
    internal fun provideUserRepository(factory: UserDataStoreFactory,
                                       mapper: UserMapper): UserRepository {
        return UserDataRepository(factory, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideUserRemote(service: CrmService,
                                   registrationEntityMapper: RegistrationEntityMapper,
                                   factory: UserEntityMapper): UserRemote {
        return UserRemoteImpl(service, factory, registrationEntityMapper)
    }

    @Provides
    @PerApplication
    internal fun provideCpRepository(factory: ContractingPartiesDataStoreFactory,
                                     mapper: crmproject.jdiploma.com.data.counterparty.mapper.CounterpartyMapper): ContractingPartiesRepository {
        return ContractingPartiesDataRepository(factory, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideCpRemote(service: CrmService,
                                 mapper: CounterpartyEntityMapper): ContractingPartiesRemote {
        return ContractingPartiesRemoteImpl(service, mapper)
    }

    @Provides
    @PerApplication
    internal fun provideCrmService(context: Context): CrmService {
        val server = context.getSharedPreferences(SETTING_KEY, 0).getString(SERVER_KEY, "")
        return CrmServiceFactory.makeCrmService(BuildConfig.DEBUG, server)
    }

    @Provides
    @PerApplication
    internal fun provideThreadExecutor(jobExecutor: JobExecutor): ThreadExecutor {
        return jobExecutor
    }


    @Provides
    @PerApplication
    internal fun providePostExecutionThread(uiThread: UiThread): PostExecutionThread {
        return uiThread
    }
}
