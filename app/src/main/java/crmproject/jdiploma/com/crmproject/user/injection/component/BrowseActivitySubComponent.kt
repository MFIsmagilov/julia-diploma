package crmproject.jdiploma.com.crmproject.user.injection.component

import crmproject.jdiploma.com.crmproject.user.browse.BrowseUserActivity
import crmproject.jdiploma.com.crmproject.user.browse.UsersFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by MaRaT on 01.03.2018.
 */
@Subcomponent
interface BrowseActivitySubComponent : AndroidInjector<BrowseUserActivity> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<BrowseUserActivity>()

}