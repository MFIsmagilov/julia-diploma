package crmproject.jdiploma.com.crmproject.dealings


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.contractingparties.CounterpartyViewModelAdapter
import crmproject.jdiploma.com.crmproject.getDealings
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_dealings.*
import javax.inject.Inject


class DealingsFragment : DaggerFragment() {

    @Inject lateinit var dealingViewModelAdapter: DealingViewModelAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_dealings, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        dealingsRecylerView.layoutManager = LinearLayoutManager(activity)
        dealingsRecylerView.adapter = dealingViewModelAdapter
        ViewCompat.setNestedScrollingEnabled(dealingsRecylerView, false)
        mSwipeRefreshLayout.setOnRefreshListener {
            dealingViewModelAdapter.dealings = getDealings()
            dealingViewModelAdapter.notifyDataSetChanged()
            //            contarctingPartiesPresenter.up()
        }

    }

    override fun onResume() {
        super.onResume()
        dealingViewModelAdapter.dealings = getDealings()
        dealingViewModelAdapter.notifyDataSetChanged()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        retainInstance = true
        (activity as AppCompatActivity).supportActionBar?.title = "Сделки"
    }
}// Required empty public constructor
