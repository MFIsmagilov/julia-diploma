package crmproject.jdiploma.com.crmproject.section.injection.component

import crmproject.jdiploma.com.crmproject.section.browse.SectionActivity
import crmproject.jdiploma.com.crmproject.user.browse.BrowseUserActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by MaRaT on 05.03.2018.
 */
@Subcomponent
interface BrowseSectionActivitiySubComponent : AndroidInjector<SectionActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<SectionActivity>()
}