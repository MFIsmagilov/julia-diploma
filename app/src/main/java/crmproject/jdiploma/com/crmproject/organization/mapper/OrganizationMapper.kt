package crmproject.jdiploma.com.crmproject.organization.mapper

import crmproject.jdiploma.com.crmproject.Mapper
import crmproject.jdiploma.com.crmproject.organization.model.OrganizationViewModel
import crmproject.jdiploma.com.presentation.organization.model.OrganizationView
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class OrganizationMapper @Inject constructor() : Mapper<OrganizationViewModel, OrganizationView> {
    override fun mapToViewModel(type: OrganizationView): OrganizationViewModel {
        return OrganizationViewModel(
                type.id,
                type.name,
                type.site
        )
    }

    override fun mapFromViewModel(type: OrganizationViewModel): OrganizationView {
        return OrganizationView(
                type.id,
                type.name,
                type.site
        )
    }

}