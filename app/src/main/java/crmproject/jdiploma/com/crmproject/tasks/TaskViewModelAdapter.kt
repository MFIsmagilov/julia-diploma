package crmproject.jdiploma.com.crmproject.tasks

import android.content.Context
import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Filter
import android.widget.TextView
import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.tasks.model.TaskViewModel
import java.text.SimpleDateFormat
import java.util.*

import javax.inject.Inject

class TaskViewModelAdapter @Inject constructor() : RecyclerView.Adapter<TaskViewModelAdapter.ViewHolder>() {

    var tasks: List<TaskViewModel> = arrayListOf()
    var context: Context? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_task, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sdf = SimpleDateFormat("dd.mm.yy")

        val task = tasks[position]
        holder.appoinmentDate.text = sdf.format(task.appoinmentDate)
        holder.dateCompletion.text = sdf.format(task.dateCompletion)
        holder.comment.text = task.comment
        if (task.status.contains("Не")) { //bad bad bad //todo: fixme
            holder.status.setBackgroundColor(Color.RED)
        } else {
            holder.status.setBackgroundColor(Color.GREEN)
        }

        if (task.user != null) {
            holder.user.text = "Назначено на: " + task.user.toString()
        } else {
            holder.user.text = "Не назначено"
        }
        holder.itemView?.setOnClickListener {
            it.let { context?.let { it.startActivity(DetailTaskActivity.getIntent(it, task)) } }
        }
    }

    override fun getItemCount(): Int {
        return tasks.size
    }

    fun showNotAssigned() {
    }

    fun filterByString(tasks: List<TaskViewModel>, constraint: CharSequence) {
        this.tasks = tasks
        SimpleFilter().filter(constraint)
    }

    fun sortByAssigned() {
        tasks = tasks.sortedBy {
            it.user == null
        }
        notifyDataSetChanged()
    }

    fun sortByNotAssigned() {
        tasks = tasks.sortedBy {
            it.user != null
        }
        notifyDataSetChanged()
    }


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var appoinmentDate: TextView = view.findViewById(R.id.dateBeginning)
        var dateCompletion: TextView = view.findViewById(R.id.dateCompletion)
        var comment: TextView = view.findViewById(R.id.taskComment)
        var user: TextView = view.findViewById(R.id.taskForUser)
        var status: View = view.findViewById(R.id.taskStatus)
    }

    inner class SimpleFilter : Filter() {
        //Этот фильтр для поиска по comment
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            val results = Filter.FilterResults()
            if (constraint == null || constraint.length === 0) {
                results.values = tasks
                results.count = tasks.size
            } else {
                val fRecords = ArrayList<TaskViewModel>()
                for (task in tasks) {
                    if (task.comment.toUpperCase().trim({ it <= ' ' }).contains(constraint.toString().toUpperCase().trim())) {
                        fRecords.add(task)
                    }
                }
                results.values = fRecords
                results.count = fRecords.size
            }
            return results
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            results?.let {
                tasks = it.values as ArrayList<TaskViewModel>
                notifyDataSetChanged()
            }
        }

    }

    inner class NotAssignedFilter : Filter() {
        override fun performFiltering(constraint: CharSequence?): FilterResults {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

        override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
            TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        }

    }
}
