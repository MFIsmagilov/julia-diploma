package crmproject.jdiploma.com.crmproject.section.mapper

import crmproject.jdiploma.com.crmproject.Mapper
import crmproject.jdiploma.com.crmproject.section.model.SectionViewModel
import crmproject.jdiploma.com.presentation.section.model.SectionView
import javax.inject.Inject

/**
 * Created by MaRaT on 05.03.2018.
 */
class SectionMapper @Inject constructor() : Mapper<SectionViewModel, SectionView> {

    override fun mapToViewModel(type: SectionView): SectionViewModel {
        return SectionViewModel(
                type.nameSec,
                type.linkSection,
                type.isDefault
        )
    }

    override fun mapFromViewModel(type: SectionViewModel): SectionView {
        return SectionView(
                type.nameSec,
                type.linkSection,
                type.isDefault
        )
    }

}