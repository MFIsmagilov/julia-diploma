package crmproject.jdiploma.com.crmproject.organization.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by MaRaT on 28.02.2018.
 */
class OrganizationViewModel(
        val id: Int,
        val name: String,
        val site: String
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeString(name)
        writeString(site)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<OrganizationViewModel> = object : Parcelable.Creator<OrganizationViewModel> {
            override fun createFromParcel(source: Parcel): OrganizationViewModel = OrganizationViewModel(source)
            override fun newArray(size: Int): Array<OrganizationViewModel?> = arrayOfNulls(size)
        }
    }
}