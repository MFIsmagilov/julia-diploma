package crmproject.jdiploma.com.crmproject.tasks

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.tasks.mapper.TaskMapper
import crmproject.jdiploma.com.presentation.task.AddTaskActivityContract
import crmproject.jdiploma.com.presentation.task.DetailActivityContract
import crmproject.jdiploma.com.presentation.task.TaskView
import crmproject.jdiploma.com.presentation.user.model.UserView
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class AddTaskActivity : DaggerAppCompatActivity(), DetailActivityContract.View {
    override fun setPresenter(presenter: DetailActivityContract.Presenter) {
        taskPresenter = presenter
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun loadUsersSucced(users: List<UserView>) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun loadUsersFailure(ex: Throwable) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createTaskSucced(task: TaskView) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun createTaskFailure(ex: Throwable) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    @Inject lateinit var taskPresenter: DetailActivityContract.Presenter
    @Inject lateinit var taskMapper: TaskMapper


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_task)
    }
}
