package crmproject.jdiploma.com.crmproject.injection.scopes

import javax.inject.Scope

/**
 * Created by MaRaT on 01.03.2018.
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class PerApplication