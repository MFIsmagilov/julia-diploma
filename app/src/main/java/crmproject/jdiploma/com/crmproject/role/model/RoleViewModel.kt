package crmproject.jdiploma.com.crmproject.role.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by MaRaT on 28.02.2018.
 */
class RoleViewModel(
        val id: Int,
        val name: String
) : Parcelable {
    override fun toString(): String {
        return name
    }

    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeString(name)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<RoleViewModel> = object : Parcelable.Creator<RoleViewModel> {
            override fun createFromParcel(source: Parcel): RoleViewModel = RoleViewModel(source)
            override fun newArray(size: Int): Array<RoleViewModel?> = arrayOfNulls(size)
        }
    }
}