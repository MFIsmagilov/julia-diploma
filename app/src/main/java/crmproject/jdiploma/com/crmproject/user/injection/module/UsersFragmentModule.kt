package crmproject.jdiploma.com.crmproject.user.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.user.browse.UsersFragment
import crmproject.jdiploma.com.domain.user.interactor.browse.UserBrowse
import crmproject.jdiploma.com.presentation.user.browse.BrowseUserContract
import crmproject.jdiploma.com.presentation.user.browse.BrowseUserPresenter
import crmproject.jdiploma.com.presentation.user.mapper.UserMapper
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 15.03.2018.
 */
@Module
open class UsersFragmentModule {

    @PerActivity
    @Provides
    internal fun provideBrowseView(usersFragment: UsersFragment): BrowseUserContract.View {
        return usersFragment
    }

    @PerActivity
    @Provides
    internal fun provideBrowsePresenter(mainView: BrowseUserContract.View,
                                        usersBrowse: UserBrowse, mapper: UserMapper):
            BrowseUserContract.Presenter {
        return BrowseUserPresenter(mainView, usersBrowse, mapper)
    }

}