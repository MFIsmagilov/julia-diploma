package crmproject.jdiploma.com.crmproject.dealings.injection.module

import crmproject.jdiploma.com.crmproject.dealings.DealingsFragment
import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by MaRaT on 03.06.2018.
 */
@Module
abstract class DealingBindingModule {
    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(BrowseDealingFragmentModule::class))
    abstract fun bindDealingFragment(): DealingsFragment
}
