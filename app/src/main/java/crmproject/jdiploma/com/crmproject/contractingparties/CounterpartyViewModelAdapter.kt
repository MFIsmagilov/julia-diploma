package crmproject.jdiploma.com.crmproject.contractingparties

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.contractingparties.model.CounterpartyViewModel
import crmproject.jdiploma.com.crmproject.tasks.TaskViewModelAdapter
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class CounterpartyViewModelAdapter @Inject constructor() : RecyclerView.Adapter<CounterpartyViewModelAdapter.ViewHolder>() {

    var contractingParties: List<CounterpartyViewModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_cp_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return contractingParties.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val counterparty = contractingParties[position]
        holder.nameCP.text = counterparty.name
        holder.addressCP.text = counterparty.address
        holder.siteCP.text = counterparty.site
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var nameCP: TextView = view.findViewById(R.id.nameCP)
        var addressCP: TextView = view.findViewById(R.id.addressCP)
        var siteCP: TextView = view.findViewById(R.id.siteCP)
    }
}