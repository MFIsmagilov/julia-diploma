package crmproject.jdiploma.com.crmproject

import android.app.Application
import crmproject.jdiploma.com.crmproject.contractingparties.ContractingPartiesFragment
import crmproject.jdiploma.com.crmproject.contractingparties.injection.module.ContractingPartiesBindingModule
import crmproject.jdiploma.com.crmproject.dealings.injection.module.DealingBindingModule
import crmproject.jdiploma.com.crmproject.user.injection.module.ActivityBindingModule
import crmproject.jdiploma.com.crmproject.injection.scopes.PerApplication
import crmproject.jdiploma.com.crmproject.login.injection.module.LoginActivityBindingModule
import crmproject.jdiploma.com.crmproject.section.injection.module.SectionActivityBindingModule
import crmproject.jdiploma.com.crmproject.tasks.injection.module.TaskFragmentBindingModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule

/**
 * Created by MaRaT on 01.03.2018.
 */
@PerApplication
@Component(modules = arrayOf(
        ActivityBindingModule::class,
        SectionActivityBindingModule::class,
        LoginActivityBindingModule::class,
        TaskFragmentBindingModule::class,
        ContractingPartiesBindingModule::class,
        DealingBindingModule::class,
        ApplicationModule::class,
        AndroidSupportInjectionModule::class))
interface ApplicationComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): ApplicationComponent
    }

    fun inject(app: CrmApplication)

}
