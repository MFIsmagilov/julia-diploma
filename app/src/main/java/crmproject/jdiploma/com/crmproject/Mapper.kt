package crmproject.jdiploma.com.crmproject

/**
 * Created by MaRaT on 28.02.2018.
 */
interface Mapper<V, D> {

    fun mapToViewModel(type: D): V

    fun mapFromViewModel(type: V): D

}