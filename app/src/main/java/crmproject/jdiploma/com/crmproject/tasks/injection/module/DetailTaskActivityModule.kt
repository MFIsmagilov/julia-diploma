package crmproject.jdiploma.com.crmproject.tasks.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.tasks.DetailTaskActivity
import crmproject.jdiploma.com.crmproject.tasks.TaskFragment
import crmproject.jdiploma.com.domain.task.interactor.browse.TaskBrowse
import crmproject.jdiploma.com.domain.user.interactor.browse.UserBrowse
import crmproject.jdiploma.com.presentation.task.BrowseTaskContract
import crmproject.jdiploma.com.presentation.task.BrowseTaskPresenter
import crmproject.jdiploma.com.presentation.task.DetailActivityContract
import crmproject.jdiploma.com.presentation.task.DetailTaskPresenter
import crmproject.jdiploma.com.presentation.user.mapper.UserMapper
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 12.03.2018.
 */
@Module
open class DetailTaskActivityModule {

    @PerActivity
    @Provides
    internal fun provideBrowseView(detailTaskActivity: DetailTaskActivity): DetailActivityContract.View {
        return detailTaskActivity
    }

    @PerActivity
    @Provides
    internal fun provideBrowsePresenter(mainView: DetailActivityContract.View,
                                        taskBrowse: TaskBrowse,
                                        userBrowse: UserBrowse,
                                        mapper: crmproject.jdiploma.com.presentation.task.TaskMapper,
                                        userMapper: UserMapper
    ):
            DetailActivityContract.Presenter {
        return DetailTaskPresenter(mainView, taskBrowse, userBrowse, mapper, userMapper)
    }
}