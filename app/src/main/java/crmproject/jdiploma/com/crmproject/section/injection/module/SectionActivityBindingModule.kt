package crmproject.jdiploma.com.crmproject.section.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.section.browse.SectionActivity
import crmproject.jdiploma.com.crmproject.user.browse.BrowseUserActivity
import crmproject.jdiploma.com.crmproject.user.injection.module.BrowseUserActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by MaRaT on 05.03.2018.
 */
@Module
abstract class SectionActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(BrowseSectionActivityModule::class))
    abstract fun bindSectionActivity(): SectionActivity

}
