package crmproject.jdiploma.com.crmproject.dealings.injection.module

import crmproject.jdiploma.com.crmproject.dealings.DealingsFragment
import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 03.06.2018.
 */
@Module
open class BrowseDealingFragmentModule {
    @PerActivity
    @Provides
    internal fun provideBrowseView(dealingsFragment: DealingsFragment): DealingsFragment {
        return dealingsFragment
    }
}