package crmproject.jdiploma.com.crmproject.tasks

import android.support.v7.widget.RecyclerView
import android.support.v4.view.ViewCompat.animate
import android.R.attr.translationY
import android.content.Context
import android.support.v4.view.ViewCompat.setTranslationY
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View


/**
 * Created by MaRaT on 14.03.2018.
 */
class MyRecyclerView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : RecyclerView(context, attrs, defStyle) {
    private var mScrollable: Boolean = false

    init {
        mScrollable = false
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        return !mScrollable || super.dispatchTouchEvent(ev)
    }

    override fun onLayout(changed: Boolean, l: Int, t: Int, r: Int, b: Int) {
        super.onLayout(changed, l, t, r, b)
        for (i in 0 until childCount) {
            animate(getChildAt(i), i)

            if (i == childCount - 1) {
                handler.postDelayed({ mScrollable = true }, (i * 100).toLong())
            }
        }
    }

    private fun animate(view: View, pos: Int) {
        view.animate().cancel()
        view.translationY = 100F
        view.alpha = 0F
        view.animate().alpha(1.0f).translationY(0F).setDuration(300).startDelay = (pos * 100).toLong()
    }
}