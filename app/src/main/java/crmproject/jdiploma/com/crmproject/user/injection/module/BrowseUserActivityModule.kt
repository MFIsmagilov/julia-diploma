package crmproject.jdiploma.com.crmproject.user.injection.module

import crmproject.jdiploma.com.crmproject.user.browse.BrowseUserActivity
import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.domain.user.interactor.browse.UserBrowse
import crmproject.jdiploma.com.presentation.user.browse.BrowseUserContract
import crmproject.jdiploma.com.presentation.user.browse.BrowseUserPresenter
import crmproject.jdiploma.com.presentation.user.mapper.UserMapper
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 01.03.2018.
 */
@Module
open class BrowseUserActivityModule {

    @PerActivity
    @Provides
    internal fun provideBrowseView(browseActivity: BrowseUserActivity): BrowseUserContract.View {
        return browseActivity
    }

    @PerActivity
    @Provides
    internal fun provideBrowsePresenter(mainView: BrowseUserContract.View,
                                        userBrowse: UserBrowse, mapper: UserMapper):
            BrowseUserContract.Presenter {
        return BrowseUserPresenter(mainView, userBrowse, mapper)
    }

}