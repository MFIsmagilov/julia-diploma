package crmproject.jdiploma.com.crmproject.contractingparties


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.contractingparties.mapper.CounterPartyMapper
import crmproject.jdiploma.com.crmproject.getContractingParties
import crmproject.jdiploma.com.presentation.counterparty.browse.BrowseCounterparty
import crmproject.jdiploma.com.presentation.counterparty.model.CounterpartyView
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_contracting_parties.*
import javax.inject.Inject


class ContractingPartiesFragment : DaggerFragment(), BrowseCounterparty.View {
    override fun setPresenter(presenter: BrowseCounterparty.Presenter) {
        contarctingPartiesPresenter = presenter
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(exception: Throwable) {
    }

    override fun showContractingParties(contractingParties: List<CounterpartyView>) {
        val cps = contractingParties.map {
            counterparty.mapToViewModel(it)
        }

        contractingPartiesAdapter.contractingParties = getContractingParties()
        contractingPartiesAdapter.notifyDataSetChanged()
    }


    @Inject lateinit var contarctingPartiesPresenter: BrowseCounterparty.Presenter
    @Inject lateinit var counterparty: CounterPartyMapper
    @Inject lateinit var contractingPartiesAdapter: CounterpartyViewModelAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_contracting_parties, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        contractingPartiesRecylerView.layoutManager = LinearLayoutManager(activity)
        contractingPartiesRecylerView.adapter = contractingPartiesAdapter
        contarctingPartiesPresenter.showContractingParties()
        ViewCompat.setNestedScrollingEnabled(contractingPartiesRecylerView, false)
        mSwipeRefreshLayout.setOnRefreshListener {
            //            contarctingPartiesPresenter.up()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        retainInstance = true
        (activity as AppCompatActivity).supportActionBar?.title = "Контрагенты"
    }

}
