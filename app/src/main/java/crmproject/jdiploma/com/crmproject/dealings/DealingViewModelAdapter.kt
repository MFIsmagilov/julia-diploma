package crmproject.jdiploma.com.crmproject.dealings

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.dealings.model.DealingViewModel
import java.text.SimpleDateFormat
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class DealingViewModelAdapter @Inject constructor() : RecyclerView.Adapter<DealingViewModelAdapter.ViewHolder>() {

    var dealings: List<DealingViewModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.fragment_dealig_item, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dealings.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val sdf = SimpleDateFormat("dd.mm.yy")
        val dealing = dealings[position]
        holder.dateBeginning.text = "01.06.18"//sdf.format(dealing.dateBegining)
        holder.dateCompletion.text = "02.06.18" //sdf.format(dealing.dateCompletion)
        holder.nameDealing.text = dealing.name
        holder.summa.text = "Сумма: " + dealing.summa.toString()
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val dateBeginning: TextView = view.findViewById(R.id.dateBeginning)
        val dateCompletion: TextView = view.findViewById(R.id.dateCompletion)
        val nameDealing: TextView = view.findViewById(R.id.nameDealing)
        val summa: TextView = view.findViewById(R.id.summa)
    }
}
