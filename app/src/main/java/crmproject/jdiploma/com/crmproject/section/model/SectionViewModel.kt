package crmproject.jdiploma.com.crmproject.section.model

/**
 * Created by MaRaT on 05.03.2018.
 */
class SectionViewModel(
        val nameSec: String,
        val linkSection: String,
        val isDefault: Boolean
)