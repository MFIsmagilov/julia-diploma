package crmproject.jdiploma.com.crmproject.tasks.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.tasks.TaskFragment
import crmproject.jdiploma.com.domain.task.interactor.browse.TaskBrowse
import crmproject.jdiploma.com.presentation.task.BrowseTaskContract
import crmproject.jdiploma.com.presentation.task.BrowseTaskPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 07.03.2018.
 */
@Module
open class BrowseTasksFragmentModule {

//    @Provides
//    internal fun provideBrowseTaskFragrement() = TaskFragment()

    @PerActivity
    @Provides
    internal fun provideBrowseView(taskFragment: TaskFragment): BrowseTaskContract.View {
        return taskFragment
    }

    @PerActivity
    @Provides
    internal fun provideBrowsePresenter(mainView: BrowseTaskContract.View,
                                        taskBrowse: TaskBrowse,
                                        mapper: crmproject.jdiploma.com.presentation.task.TaskMapper
    ):
            BrowseTaskContract.Presenter {
        return BrowseTaskPresenter(mainView, taskBrowse, mapper)
    }

}