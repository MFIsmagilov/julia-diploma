package crmproject.jdiploma.com.crmproject.login.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.login.LoginActivity
import crmproject.jdiploma.com.crmproject.section.browse.SectionActivity
import crmproject.jdiploma.com.crmproject.section.injection.module.BrowseSectionActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by MaRaT on 03.06.2018.
 */
@Module
abstract class LoginActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(BrowseSectionActivityModule::class))
    abstract fun bindSectionActivity(): LoginActivity

}