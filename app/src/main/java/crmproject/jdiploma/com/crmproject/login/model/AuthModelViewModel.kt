package crmproject.jdiploma.com.crmproject.login.model

/**
 * Created by MaRaT on 03.06.2018.
 */
class AuthModelViewModel(
        val username: String,
        val password: String
)