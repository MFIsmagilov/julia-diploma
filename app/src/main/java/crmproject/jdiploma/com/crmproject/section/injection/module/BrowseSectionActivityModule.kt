package crmproject.jdiploma.com.crmproject.section.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.section.browse.SectionActivity
import crmproject.jdiploma.com.presentation.section.mapper.SectionMapper
import crmproject.jdiploma.com.domain.section.interactor.browse.SectionBrowse
import crmproject.jdiploma.com.domain.user.interactor.UserSingleUseCase
import crmproject.jdiploma.com.domain.user.interactor.browse.UserBrowse
import crmproject.jdiploma.com.domain.user.user.User
import crmproject.jdiploma.com.presentation.section.browse.BrowseSectionContract
import crmproject.jdiploma.com.presentation.section.browse.BrowseSectionPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 05.03.2018.
 */
@Module
open class BrowseSectionActivityModule {

    @PerActivity
    @Provides
    internal fun provideBrowseView(sectionActivity: SectionActivity): BrowseSectionContract.View {
        return sectionActivity
    }

    @PerActivity
    @Provides
    internal fun provideBrowsePresenter(mainView: BrowseSectionContract.View,
                                        sectionBrowse: SectionBrowse,
                                        mapper: SectionMapper,
                                        userSingleUseCase: UserBrowse,
                                        userMapper: crmproject.jdiploma.com.presentation.user.mapper.UserMapper
    ):
            BrowseSectionContract.Presenter {
        return BrowseSectionPresenter(mainView, sectionBrowse, mapper, userSingleUseCase, userMapper)
    }

}