package crmproject.jdiploma.com.crmproject.login

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.app.LoaderManager.LoaderCallbacks
import android.content.CursorLoader
import android.content.Loader
import android.database.Cursor
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.os.Bundle
import android.provider.ContactsContract
import android.text.TextUtils
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.TextView

import java.util.ArrayList
import android.Manifest.permission.READ_CONTACTS
import crmproject.jdiploma.com.crmproject.*
import crmproject.jdiploma.com.crmproject.section.browse.SectionActivity
import crmproject.jdiploma.com.remote.CrmService
import crmproject.jdiploma.com.remote.CrmServiceFactory
import dagger.android.support.DaggerAppCompatActivity

import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        email_sign_in_button.setOnClickListener {
            if (server_url.text.isNotBlank() && email.text.isNotBlank() && password.text.isNotBlank()) {
                val pref = getSharedPreferences(SETTING_KEY, 0).edit()

                pref.putString(SERVER_KEY, server_url.text.toString())
                pref.putString(USERNAME_KEY, email.text.toString())
                pref.putString(PASSWORD_KEY, password.text.toString())
                pref.apply()

                startActivity(SectionActivity.getIntent(this))
            }
        }
    }
}
