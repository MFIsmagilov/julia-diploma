package crmproject.jdiploma.com.crmproject.user.browse

import android.content.Context
import android.support.transition.TransitionManager
import android.support.v7.widget.RecyclerView
import android.util.AttributeSet
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.user.model.UserViewModel
import javax.inject.Inject

/**
 * Created by MaRaT on 15.03.2018.
 */
class UserRecylerList @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, defStyle: Int = 0) : RecyclerView(context, attrs, defStyle)

class UserRecylerApadpet @Inject constructor() : RecyclerView.Adapter<UserRecylerApadpet.UserViewHolder>() {
    inner class UserViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        var fullName: TextView = view.findViewById(R.id.item_userfullname)
        var email: TextView = view.findViewById(R.id.sub_item_email)
        var role: TextView = view.findViewById(R.id.sub_item_role)
        var position: TextView = view.findViewById(R.id.sub_item_position)
        var lastLogin: TextView = view.findViewById(R.id.sub_item_last_login)

        var subItem: View = view.findViewById(R.id.sub_item)

        fun bind(user: UserViewHolderModel) {
            val expanded = user.isExpanded
            subItem.visibility = if (expanded) View.VISIBLE else View.GONE

            fullName.text = user.getFullName()
            email.text = user.getEmail()
            role.text = user.getRole()
            position.text = user.getPosition()
            lastLogin.text = user.getLastLogin()
        }
    }

    inner class UserViewHolderModel(val userViewModel: UserViewModel, var isExpanded: Boolean) {
        fun getFullName(): String {
            return userViewModel.firstName + " " + userViewModel.lastName
        }

        fun getEmail(): String {
            return userViewModel.email
        }

        fun getRole(): String {
            return userViewModel.role.toString()
        }

        fun getPosition(): String {
            return userViewModel.position
        }

        fun getLastLogin(): String {
            return userViewModel.lastLogin?.toString() ?: ""
        }
    }

    private var users: List<UserViewHolderModel> = arrayListOf()

    fun setUsers(users: List<UserViewModel>) {
        this.users = users.map {
            UserViewHolderModel(it, false)
        }
    }

    override fun onBindViewHolder(holder: UserViewHolder?, position: Int) {

        val user = users[position]
        holder?.bind(user)
        holder?.fullName?.setOnClickListener {
            val expanded = user.isExpanded
            user.isExpanded = !expanded
//            TransitionManager.beginDelayedTransition(holder.itemView.findViewById(R.id.usersListRecylerView))
            notifyDataSetChanged()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): UserViewHolder {
        val view = LayoutInflater.from(parent?.context)
                .inflate(R.layout.fragment_user_item, parent, false)
        return UserViewHolder(view)
    }

    override fun getItemCount(): Int {
        return users.size
    }
}