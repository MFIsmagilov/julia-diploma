package  crmproject.jdiploma.com.crmproject.user.model

import android.os.Parcel
import android.os.Parcelable
import crmproject.jdiploma.com.crmproject.organization.model.OrganizationViewModel
import crmproject.jdiploma.com.crmproject.role.model.RoleViewModel
import java.io.Serializable
import java.util.*

/**
 * Created by MaRaT on 28.02.2018.
 */

class UserViewModel(
        val id: Int,
        val username: String,
        val firstName: String,
        val lastName: String,
        val email: String,
        val isSuperuser: Boolean,
        val lastLogin: Date?,
        val role: RoleViewModel,
        val organization: OrganizationViewModel,
        val position: String
) : Parcelable {
    override fun toString(): String {

        var fullname = "$firstName $lastName"
        if (fullname.isNotBlank()) {
            fullname = "($fullname)"
        }
        return "$username $fullname"
    }

    constructor(source: Parcel) : this(
            source.readInt(),
            source.readString(),
            source.readString(),
            source.readString(),
            source.readString(),
            1 == source.readInt(),
            source.readSerializable() as Date?,
            source.readParcelable<RoleViewModel>(RoleViewModel::class.java.classLoader),
            source.readParcelable<OrganizationViewModel>(OrganizationViewModel::class.java.classLoader),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeInt(id)
        writeString(username)
        writeString(firstName)
        writeString(lastName)
        writeString(email)
        writeInt((if (isSuperuser) 1 else 0))
        writeSerializable(lastLogin)
        writeParcelable(role, 0)
        writeParcelable(organization, 0)
        writeString(position)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<UserViewModel> = object : Parcelable.Creator<UserViewModel> {
            override fun createFromParcel(source: Parcel): UserViewModel = UserViewModel(source)
            override fun newArray(size: Int): Array<UserViewModel?> = arrayOfNulls(size)
        }
    }
}