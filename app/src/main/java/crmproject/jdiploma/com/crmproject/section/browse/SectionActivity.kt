package crmproject.jdiploma.com.crmproject.section.browse


import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.NavigationView
import android.support.v4.app.Fragment
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.contractingparties.ContractingPartiesFragment
import crmproject.jdiploma.com.crmproject.dealings.DealingsFragment
import crmproject.jdiploma.com.crmproject.getUser
import crmproject.jdiploma.com.crmproject.section.mapper.SectionMapper
import crmproject.jdiploma.com.crmproject.tasks.DetailTaskActivity
import crmproject.jdiploma.com.crmproject.tasks.TaskFragment
import crmproject.jdiploma.com.crmproject.toast
import crmproject.jdiploma.com.crmproject.user.browse.BrowseUserActivity
import crmproject.jdiploma.com.crmproject.user.browse.UsersFragment
import crmproject.jdiploma.com.crmproject.user.mapper.UserMapper
import crmproject.jdiploma.com.presentation.section.browse.BrowseSectionContract
import crmproject.jdiploma.com.presentation.section.model.SectionView
import crmproject.jdiploma.com.presentation.user.model.UserView
import dagger.android.AndroidInjection
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.nav_header_main.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import javax.inject.Inject


//todo: rename to main activity
class SectionActivity : DaggerAppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, BrowseSectionContract.View {

    companion object {
        val TAG = SectionActivity::class.java.simpleName

        fun getIntent(context: Context): Intent {
            val intent = Intent(context, SectionActivity::class.java)
            return intent
        }
    }

    override fun showUser(user: UserView) {

//        val user = userMapper.mapToViewModel(user)
        val user = getUser()
        Log.d("$TAG", user.toString())
        if (nav_header_name != null) {
            nav_header_name.text = user.firstName + " " + user.lastName
            nav_header_email.text = user.email
            nav_header_name.setOnClickListener {
                startActivity(BrowseUserActivity.getIntent(this@SectionActivity))
            }
            nav_header_user_info_progressbar.visibility = View.GONE
        }
    }

    @Inject lateinit var onboardingPresenter: BrowseSectionContract.Presenter
    @Inject lateinit var sectionMapper: SectionMapper
    @Inject lateinit var userMapper: UserMapper

//    @Inject lateinit var tasksFragment: DispatchingAndroidInjector<TaskFragment>

    override fun showSection(section: List<SectionView>) {

        val sections = section.map { sectionMapper.mapToViewModel(it) }

        //todo: dynamicaly create nav menu
    }

    override fun setPresenter(presenter: BrowseSectionContract.Presenter) {
        onboardingPresenter = presenter
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showError(exception: Throwable) {
        toast(this, exception.message ?: "?")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

//        fab.setOnClickListener {
//            startActivity(DetailTaskActivity.getIntent(this@SectionActivity))
//        }

        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)
        nav_view.menu.getItem(0).isChecked = true

        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                    .replace(R.id.content_container, TaskFragment())
                    .commit()
        }
        onboardingPresenter.getSections()
        onboardingPresenter.getUser(24)
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            android.R.id.home -> {
                onBackPressed(); return true
            }
            else ->
                return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_tasks -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.content_container, TaskFragment())
                        .commit()
                // Handle the camera action
            }
            R.id.nav_dealings -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.content_container, DealingsFragment())
                        .commit()
            }
            R.id.nav_contragents -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.content_container, ContractingPartiesFragment())
                        .commit()
            }
            R.id.nav_employees -> {
                supportFragmentManager.beginTransaction()
                        .replace(R.id.content_container, UsersFragment())
                        .commit()
            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }
}
