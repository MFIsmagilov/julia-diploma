package crmproject.jdiploma.com.crmproject.tasks.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.tasks.AddTaskActivity
import crmproject.jdiploma.com.domain.task.interactor.browse.TaskBrowse
import crmproject.jdiploma.com.presentation.task.AddTaskActivityContract
import crmproject.jdiploma.com.presentation.task.AddTaskPresenter
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 12.03.2018.
 */
//@Module
//open class AddTaskActivityModule {
//
//    @PerActivity
//    @Provides
//    internal fun provideBrowseView(addTaskActivity: AddTaskActivity): AddTaskActivityContract.View {
//        return addTaskActivity
//    }
//
//    @PerActivity
//    @Provides
//    internal fun provideBrowsePresenter(mainView: AddTaskActivityContract.View,
//                                        taskBrowse: TaskBrowse,
//                                        mapper: crmproject.jdiploma.com.presentation.task.TaskMapper
//                                        ):
//            AddTaskActivityContract.Presenter {
//        return AddTaskPresenter(mainView, taskBrowse, mapper)
//    }
//}