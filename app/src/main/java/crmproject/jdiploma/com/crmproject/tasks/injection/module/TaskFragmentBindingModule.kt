package crmproject.jdiploma.com.crmproject.tasks.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.tasks.DetailTaskActivity
import crmproject.jdiploma.com.crmproject.tasks.TaskFragment
import crmproject.jdiploma.com.crmproject.tasks.injection.component.BrowseTasksFragmentSubComponent
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by MaRaT on 07.03.2018.
 */
@Module
abstract class TaskFragmentBindingModule {//todo: rename

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(BrowseTasksFragmentModule::class))
    abstract fun bindTaskFragment(): TaskFragment

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(DetailTaskActivityModule::class))
    abstract fun bindDetailActivity(): DetailTaskActivity
}
