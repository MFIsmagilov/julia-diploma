package crmproject.jdiploma.com.crmproject.user.browse


import android.os.Bundle
import android.support.v4.view.ViewCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.tasks.TaskViewModelAdapter
import crmproject.jdiploma.com.crmproject.user.mapper.UserMapper
import crmproject.jdiploma.com.presentation.user.browse.BrowseUserContract
import crmproject.jdiploma.com.presentation.user.model.UserView
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_users.*
import javax.inject.Inject
import android.support.v7.widget.SimpleItemAnimator
import crmproject.jdiploma.com.crmproject.getUsers


class UsersFragment : DaggerFragment(), BrowseUserContract.View {
    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showUserData(user: UserView) {
    }

    override fun showError(exception: Throwable) {
    }

    override fun showUsers(users: List<UserView>) {
        val usersVm1 = users.map {
            userMapper.mapToViewModel(it)
        }
        val usersVm = getUsers()
        usersAdapter.setUsers(usersVm)
        usersAdapter.notifyDataSetChanged()
    }

    @Inject lateinit var usersPresenter: BrowseUserContract.Presenter
    @Inject lateinit var userMapper: UserMapper
    @Inject lateinit var usersAdapter: UserRecylerApadpet
    override fun setPresenter(presenter: BrowseUserContract.Presenter) {
        usersPresenter = presenter
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.employees)
        return inflater.inflate(R.layout.fragment_users, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        (usersListRecylerView.itemAnimator as SimpleItemAnimator).supportsChangeAnimations = false
        usersListRecylerView.layoutManager = LinearLayoutManager(activity)
        usersListRecylerView.adapter = usersAdapter
        usersPresenter.showUsers()

        ViewCompat.setNestedScrollingEnabled(usersListRecylerView, false)
    }
}
