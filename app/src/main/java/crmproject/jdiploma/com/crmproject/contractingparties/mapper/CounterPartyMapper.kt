package crmproject.jdiploma.com.crmproject.contractingparties.mapper

import crmproject.jdiploma.com.crmproject.Mapper
import crmproject.jdiploma.com.crmproject.contractingparties.model.CounterpartyViewModel
import crmproject.jdiploma.com.presentation.counterparty.model.CounterpartyView
import javax.inject.Inject

/**
 * Created by MaRaT on 03.06.2018.
 */
class CounterPartyMapper @Inject constructor(
) : Mapper<CounterpartyViewModel, CounterpartyView> {
    override fun mapToViewModel(type: CounterpartyView): CounterpartyViewModel {
        return CounterpartyViewModel(
                type.id,
                type.name,
                type.address,
                type.site
        )
    }

    override fun mapFromViewModel(type: CounterpartyViewModel): CounterpartyView {
        return CounterpartyView(
                type.id,
                type.name,
                type.address,
                type.site
        )
    }
}