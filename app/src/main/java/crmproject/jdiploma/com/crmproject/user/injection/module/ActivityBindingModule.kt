package crmproject.jdiploma.com.crmproject.user.injection.module

import crmproject.jdiploma.com.crmproject.user.browse.BrowseUserActivity
import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.user.browse.UsersFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by MaRaT on 01.03.2018.
 */

@Module
abstract class ActivityBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(BrowseUserActivityModule::class))
    abstract fun bindMainActivity(): BrowseUserActivity

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(UsersFragmentModule::class))
    abstract fun bindUsersFragment(): UsersFragment

}