package crmproject.jdiploma.com.crmproject.user.browse

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.toast
import crmproject.jdiploma.com.crmproject.user.mapper.UserMapper
import crmproject.jdiploma.com.presentation.user.browse.BrowseUserContract
import crmproject.jdiploma.com.presentation.user.model.UserView
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.content_scrolling.user_position_text as userPositionTextView
import kotlinx.android.synthetic.main.content_scrolling.user_email as userEmailTextView
import kotlinx.android.synthetic.main.content_scrolling.user_role as userRoleTextView

import javax.inject.Inject
import android.support.design.widget.Snackbar
import android.support.design.widget.FloatingActionButton
import android.widget.Toolbar
import crmproject.jdiploma.com.crmproject.R.id.fab
import crmproject.jdiploma.com.crmproject.getUser
import crmproject.jdiploma.com.crmproject.tasks.DetailTaskActivity
import kotlinx.android.synthetic.main.activity_browse_user.*


class BrowseUserActivity : AppCompatActivity(), BrowseUserContract.View {
    override fun showUsers(users: List<UserView>) {
        throw UnsupportedOperationException()
    }

    companion object {
        fun getIntent(context: Context): Intent {
            val intent = Intent(context, BrowseUserActivity::class.java)
            return intent
        }
    }

    @Inject lateinit var onboardingPresenter: BrowseUserContract.Presenter
    @Inject lateinit var userMapper: UserMapper

    override fun setPresenter(presenter: BrowseUserContract.Presenter) {
        onboardingPresenter = presenter
    }

    override fun showProgress() {
    }

    override fun hideProgress() {
    }

    override fun showUserData(user: UserView) {
//        val user = userMapper.mapToViewModel(user)
        val user = getUser()
        main_collapsing.title = user.firstName + " " + user.lastName
        userRoleTextView.text = user.role.toString()
        userPositionTextView.text = user.position
        if (user.email.isNotEmpty()) {
            userEmailTextView.text = user.email
        } else {
            userEmailTextView.text = "Does not exist\n"
        }
    }

    override fun showError(exception: Throwable) {
        toast(this, exception.message ?: "?")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setTheme(R.style.AppTheme_NoActionBar_BrowseUserActivity)
        super.onCreate(savedInstanceState)
        AndroidInjection.inject(this)
        setContentView(R.layout.activity_browse_user)
        onboardingPresenter.showUserById(24)
    }

    override fun onStop() {
        super.onStop()
        onboardingPresenter.stop()
    }
}
