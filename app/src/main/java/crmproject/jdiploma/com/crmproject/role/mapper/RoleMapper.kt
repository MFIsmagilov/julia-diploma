package crmproject.jdiploma.com.crmproject.role.mapper

import crmproject.jdiploma.com.crmproject.Mapper
import crmproject.jdiploma.com.crmproject.role.model.RoleViewModel
import crmproject.jdiploma.com.presentation.role.model.RoleView
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */
class RoleMapper @Inject constructor(): Mapper<RoleViewModel, RoleView> {

    override fun mapToViewModel(type: RoleView): RoleViewModel {
        return RoleViewModel(
                type.id,
                type.name
        )
    }

    override fun mapFromViewModel(type: RoleViewModel): RoleView {
        return RoleView(
                type.id,
                type.name
        )
    }
}