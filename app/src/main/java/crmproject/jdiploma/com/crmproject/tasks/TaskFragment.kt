package crmproject.jdiploma.com.crmproject.tasks

import android.app.ActivityOptions
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v4.view.ViewCompat
import android.support.v7.widget.LinearLayoutManager
import android.view.*

import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.tasks.mapper.TaskMapper
import crmproject.jdiploma.com.crmproject.toast
import crmproject.jdiploma.com.presentation.task.BrowseTaskContract
import crmproject.jdiploma.com.presentation.task.TaskView
import dagger.android.support.DaggerFragment
import kotlinx.android.synthetic.main.fragment_task_list.*
import javax.inject.Inject
import android.support.v7.widget.PopupMenu
import android.support.v7.widget.SearchView
import crmproject.jdiploma.com.crmproject.tasks.model.TaskViewModel
import kotlinx.android.synthetic.main.app_bar_main.*
import android.support.v7.app.AppCompatActivity
import crmproject.jdiploma.com.crmproject.getTasks

class TaskFragment : DaggerFragment(), BrowseTaskContract.View {
    override fun succeedLoaded() {
        mNoTasks.visibility = View.GONE
        tasksListRecylerView.visibility = View.VISIBLE
    }

    override fun failedLoaded() {
//        tasksListRecylerView.visibility = View.GONE
//        mNoTasks.visibility = View.VISIBLE
    }


    override fun showProgress() {
        mSwipeRefreshLayout.isRefreshing = true
    }

    override fun hideProgress() {
        mSwipeRefreshLayout.isRefreshing = false
    }

    override fun showTasksData(taskView: List<TaskView>) {
        if (taskView.isEmpty()) {
            tasksListRecylerView.visibility = View.GONE
            mNoTasks.visibility = View.VISIBLE
            return
        } else {
            tasksListRecylerView.visibility = View.VISIBLE
            mNoTasks.visibility = View.GONE
        }

//        tasks = taskView.map { taskMapper.mapToViewModel(it) }
        tasks = getTasks()
        tasks?.let {
            taskAdapter.context = activity
            taskAdapter.tasks = it
            taskAdapter.notifyDataSetChanged()
        }
    }

    override fun showError(exception: Throwable) {
        toast(activity as Context, exception.message ?: "?")
    }

    override fun taskCreatedSucceed(task: TaskView) {

    }

    override fun setPresenter(presenter: BrowseTaskContract.Presenter) {
        taskPresenter = presenter
    }

    @Inject lateinit var taskPresenter: BrowseTaskContract.Presenter
    @Inject lateinit var taskMapper: TaskMapper
    @Inject lateinit var taskAdapter: TaskViewModelAdapter


    private var filterPopupMenu: PopupMenu? = null
    private var tasks: List<TaskViewModel>? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_task_list, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        tasksListRecylerView.layoutManager = LinearLayoutManager(activity)
        tasksListRecylerView.adapter = taskAdapter
        taskPresenter.showTasks()
        ViewCompat.setNestedScrollingEnabled(tasksListRecylerView, false)
        mSwipeRefreshLayout.setOnRefreshListener {
            taskPresenter.updateTasks()
        }
        context?.let { activity?.fab?.setImageDrawable(ContextCompat.getDrawable(it, R.drawable.ic_add)) }
        activity?.fab?.setOnClickListener {
            activity?.let {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    it.startActivity(DetailTaskActivity.getIntent(it),
                            ActivityOptions.makeSceneTransitionAnimation(it).toBundle()
                    )
                } else {
                    it.startActivity(DetailTaskActivity.getIntent(it))
                    it.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out)
                }
            }
        }
//        tasksListRecylerView.isNestedScrollingEnabled = false
//        ViewCompat.setNestedScrollingEnabled(tasksListRecylerView, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
        retainInstance = true
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.task)
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        menu?.clear()
        inflater?.inflate(R.menu.tasks_fragment_menu, menu)
        menu?.let {
            doSearchMenuItem(it)
        }
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun doSearchMenuItem(menu: Menu) {
        val menuItem = menu.findItem(R.id.mtf_action_search)
        val searchView = menuItem.actionView as SearchView

        searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                newText?.let {
                    tasks?.let {
                        taskAdapter.filterByString(it, newText)
                    }
                }
                return false
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        return when (item?.itemId) {
            R.id.mtf_action_filter -> {
                val menuItemView = activity?.findViewById<View>(R.id.mtf_action_filter)
                menuItemView?.let {
                    if (filterPopupMenu == null) {
                        filterPopupMenu = PopupMenu(activity as Context, menuItemView)
                        filterPopupMenu?.inflate(R.menu.tasks_fragment_filter_popup_menu)
                        filterPopupMenu?.setOnMenuItemClickListener { item ->
                            when (item?.itemId) {
                                R.id.mtf_action_assigned -> {
                                    item.isChecked = true
                                    taskAdapter.sortByAssigned()
                                    true
                                }
                                R.id.mtf_action_not_assigned -> {
                                    item.isChecked = true
                                    taskAdapter.sortByNotAssigned()
                                    true
                                }
                                else -> {
                                    false
                                }
                            }
                        }

                    }
                    filterPopupMenu?.show()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


}
