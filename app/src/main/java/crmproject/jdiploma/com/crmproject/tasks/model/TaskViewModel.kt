package crmproject.jdiploma.com.crmproject.tasks.model

import android.os.Parcel
import android.os.Parcelable
import crmproject.jdiploma.com.crmproject.user.model.UserViewModel
import java.io.Serializable
import java.util.*

/**
 * Created by MaRaT on 07.03.2018.
 */
class TaskViewModel(
        val appoinmentDate: Date,
        val dateCompletion: Date,
        val comment: String,
        val user: UserViewModel?,
        val status: String
) : Parcelable {
    constructor(source: Parcel) : this(
            source.readSerializable() as Date,
            source.readSerializable() as Date,
            source.readString(),
            source.readParcelable<UserViewModel>(UserViewModel::class.java.classLoader),
            source.readString()
    )

    override fun describeContents() = 0

    override fun writeToParcel(dest: Parcel, flags: Int) = with(dest) {
        writeSerializable(appoinmentDate)
        writeSerializable(dateCompletion)
        writeString(comment)
        writeParcelable(user, 0)
        writeString(status)
    }

    companion object {
        @JvmField
        val CREATOR: Parcelable.Creator<TaskViewModel> = object : Parcelable.Creator<TaskViewModel> {
            override fun createFromParcel(source: Parcel): TaskViewModel = TaskViewModel(source)
            override fun newArray(size: Int): Array<TaskViewModel?> = arrayOfNulls(size)
        }
    }
}