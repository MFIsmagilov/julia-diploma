package crmproject.jdiploma.com.crmproject.tasks

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
//import com.jakewharton.rxbinding2.widget.RxAdapterView
//import com.jakewharton.rxbinding2.widget.RxTextView
import crmproject.jdiploma.com.crmproject.R
import crmproject.jdiploma.com.crmproject.tasks.mapper.TaskMapper
import crmproject.jdiploma.com.crmproject.tasks.model.TaskViewModel
import crmproject.jdiploma.com.crmproject.toast
import crmproject.jdiploma.com.crmproject.user.mapper.UserMapper
import crmproject.jdiploma.com.crmproject.user.model.UserViewModel
import crmproject.jdiploma.com.domain.user.user.User
import crmproject.jdiploma.com.presentation.task.DetailActivityContract
import crmproject.jdiploma.com.presentation.task.DetailTaskPresenter
import crmproject.jdiploma.com.presentation.task.TaskView
import crmproject.jdiploma.com.presentation.user.model.UserView
import dagger.android.support.DaggerAppCompatActivity
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.activity_detail_task.*
import kotlinx.android.synthetic.main.task_detail.*
import java.text.SimpleDateFormat
import javax.inject.Inject
import io.reactivex.functions.Function5
import android.R.attr.startYear
import android.app.DatePickerDialog
import android.widget.*
import com.jakewharton.rxbinding2.widget.RxAdapterView
import com.jakewharton.rxbinding2.widget.RxTextView
import crmproject.jdiploma.com.crmproject.getUsers
import crmproject.jdiploma.com.crmproject.snackbarWithButton
import java.util.*
import kotlin.concurrent.thread


class DetailTaskActivity : DaggerAppCompatActivity(), DetailActivityContract.View {

    @Inject lateinit var taskPresenter: DetailActivityContract.Presenter
    @Inject lateinit var taskMapper: TaskMapper
    @Inject lateinit var userMapper: UserMapper
    private lateinit var commentTaskChangeObservable: Observable<CharSequence>
    private lateinit var appoinmentDateChangeObservable: Observable<CharSequence>
    private lateinit var completionDateChangeObservable: Observable<CharSequence>
    private lateinit var statusChangeObservable: Observable<CharSequence>
    private lateinit var userChangeObservable: Observable<Int>
    private lateinit var creatingTaskViewModel: TaskViewModel //объект задачи которые отправим на сервер для создания

    private val sdf = SimpleDateFormat("dd.mm.yy")
    override fun createTaskSucced(task: TaskView) {
        fabProgressCircle.hide()
    }

    override fun createTaskFailure(ex: Throwable) {
        snackbarWithButton(fab1, "Сould not create task")
        fabProgressCircle.hide()
    }

    override fun setPresenter(presenter: DetailActivityContract.Presenter) {
        taskPresenter = presenter
    }

    override fun showProgress() {
        userLoadProgressBar.visibility = View.VISIBLE
    }

    override fun hideProgress() {
        userLoadProgressBar.visibility = View.GONE
    }

    override fun loadUsersSucced(users: List<UserView>) {
        if (users.isEmpty()) {
            return
        }
//        val us = users.map { userMapper.mapToViewModel(it) }
//        Log.d(TAG, us[0].toString())
        val us = getUsers()
        users_spinner.adapter = MySpinnerAdapter(this, us)
    }

    override fun loadUsersFailure(ex: Throwable) {
        ex.message?.let { toast(this, it) }
    }

    companion object {
        val TAG = "DetailTaskActivity"
        val TASK_INTENT = "task_intent"
        fun getIntent(context: Context, task: TaskViewModel): Intent {
            val intent = Intent(context, DetailTaskActivity::class.java)
            intent.putExtra(TASK_INTENT, task)
            return intent
        }

        fun getIntent(context: Context): Intent {
            val intent = Intent(context, DetailTaskActivity::class.java)
            return intent
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail_task)
        taskPresenter.showUsers()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val taskVewModel = intent?.getParcelableExtra<TaskViewModel>(TASK_INTENT)
        if (taskVewModel != null) {
            supportActionBar?.title = "Задача"
            comment_tiet.setText(taskVewModel.comment)
            appoinment_date_tiet.setText(sdf.format(taskVewModel.appoinmentDate))
            completion_date_tiet.setText(sdf.format(taskVewModel.dateCompletion))
            status_tiet.setText(taskVewModel.status)

            fab1.visibility = View.GONE
        } else {
            supportActionBar?.title = "Создать задачу"

            commentTaskChangeObservable = RxTextView.textChanges(comment_tiet)
            appoinmentDateChangeObservable = RxTextView.textChanges(appoinment_date_tiet)
            appoinment_date_tiet.setOnFocusChangeListener { v, hasFocus ->
                if(hasFocus) createDatePicker(v as TextView).show()
            }
            completionDateChangeObservable = RxTextView.textChanges(completion_date_tiet)
            completion_date_tiet.setOnFocusChangeListener { v, hasFocus ->
                if(hasFocus) createDatePicker(v as TextView).show()
            }
            statusChangeObservable = RxTextView.textChanges(status_tiet)
            userChangeObservable = RxAdapterView.itemSelections(users_spinner)
            combineLatestEvents()
            fab1.visibility = View.VISIBLE
            fab1.isEnabled = false
            fab1.setOnClickListener {
                fabProgressCircle.show()
                taskPresenter.createTask(
                        taskMapper.mapFromViewModel(creatingTaskViewModel)
                )
            }
        }
    }


    fun combineLatestEvents() {

        Observable.combineLatest(
                //проверка что все заполнено
                commentTaskChangeObservable.skip(1),
                appoinmentDateChangeObservable.skip(1),
                completionDateChangeObservable.skip(1),
                statusChangeObservable.skip(1),
                userChangeObservable.skip(1),
                Function5<CharSequence, CharSequence, CharSequence, CharSequence, Int, TaskViewModel> { comment, appoinmentDate, completionDate, status, userItemIndex ->
                    run {
                        TaskViewModel(
                                sdf.parse(appoinmentDate.toString()),
                                sdf.parse(completionDate.toString()),
                                comment.toString(),
                                users_spinner.getItemAtPosition(userItemIndex) as UserViewModel,
                                status.toString()
                        )
                    }
                }
        )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe {
                    fab1.isEnabled = true
                    creatingTaskViewModel = it
                }
    }

    fun createDatePicker(viewForSetDate: TextView): DatePickerDialog {
        val calendar = Calendar.getInstance()
        return DatePickerDialog(this@DetailTaskActivity, DatePickerDialog.OnDateSetListener { view, year, month, dayOfMonth -> viewForSetDate.text = "$dayOfMonth.$month.$year" },
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_WEEK))
    }

    class MySpinnerAdapter(context: Context, users: List<UserViewModel>) : ArrayAdapter<UserViewModel>(context, android.R.layout.simple_spinner_item, users)

}

