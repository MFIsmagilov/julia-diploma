package crmproject.jdiploma.com.crmproject

import android.content.Context
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Toast

/**
 * Created by MaRaT on 03.03.2018.
 */

fun toast(context: Context, message: String) {
    Toast.makeText(context, message, Toast.LENGTH_LONG).show()
}

fun snackbarWithButton(view: View, message: String) {

    val snackbar = Snackbar.make(view, message, Snackbar.LENGTH_INDEFINITE)
    snackbar.setAction("OK") { snackbar.dismiss() }
    snackbar.show()
}