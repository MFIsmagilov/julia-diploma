package crmproject.jdiploma.com.crmproject.tasks.mapper

import crmproject.jdiploma.com.crmproject.Mapper
import crmproject.jdiploma.com.crmproject.tasks.model.TaskViewModel
import crmproject.jdiploma.com.crmproject.user.mapper.UserMapper
import crmproject.jdiploma.com.presentation.task.TaskView
import javax.inject.Inject

/**
 * Created by MaRaT on 07.03.2018.
 */
class TaskMapper @Inject constructor(
        private val userMapper: UserMapper
) : Mapper<TaskViewModel, TaskView> {
    override fun mapToViewModel(type: TaskView): TaskViewModel {
        return TaskViewModel(
                type.appoinmentDate,
                type.dateCompletion,
                type.comment,
                type.user?.let { userMapper.mapToViewModel(it) },
                type.status
        )
    }

    override fun mapFromViewModel(type: TaskViewModel): TaskView {
        return TaskView(
                type.appoinmentDate,
                type.dateCompletion,
                type.comment,
                type.user?.let { userMapper.mapFromViewModel(it) },
                type.status
        )
    }
}