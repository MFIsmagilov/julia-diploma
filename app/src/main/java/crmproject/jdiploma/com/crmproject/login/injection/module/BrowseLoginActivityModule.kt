package crmproject.jdiploma.com.crmproject.login.injection.module

import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.login.LoginActivity
import crmproject.jdiploma.com.crmproject.section.browse.SectionActivity
import crmproject.jdiploma.com.domain.section.interactor.browse.SectionBrowse
import crmproject.jdiploma.com.domain.user.interactor.browse.UserBrowse
import crmproject.jdiploma.com.presentation.section.browse.BrowseSectionContract
import crmproject.jdiploma.com.presentation.section.browse.BrowseSectionPresenter
import crmproject.jdiploma.com.presentation.section.mapper.SectionMapper
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 03.06.2018.
 */
@Module
open class BrowseLoginActivityModule {

    @PerActivity
    @Provides
    internal fun provideBrowseView(loginActivity: LoginActivity): LoginActivity {
        return loginActivity
    }
}