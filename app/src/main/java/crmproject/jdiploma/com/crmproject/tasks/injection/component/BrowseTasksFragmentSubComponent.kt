package crmproject.jdiploma.com.crmproject.tasks.injection.component

import crmproject.jdiploma.com.crmproject.tasks.TaskFragment
import crmproject.jdiploma.com.crmproject.tasks.injection.module.BrowseTasksFragmentModule
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by MaRaT on 07.03.2018.
 */
@Subcomponent(modules = arrayOf(
        BrowseTasksFragmentModule::class
))
interface BrowseTasksFragmentSubComponent : AndroidInjector<TaskFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<TaskFragment>()
}