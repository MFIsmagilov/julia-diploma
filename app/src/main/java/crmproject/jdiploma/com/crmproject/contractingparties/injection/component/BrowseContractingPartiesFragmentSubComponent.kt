package crmproject.jdiploma.com.crmproject.contractingparties.injection.component

import crmproject.jdiploma.com.crmproject.contractingparties.ContractingPartiesFragment
import crmproject.jdiploma.com.crmproject.contractingparties.injection.module.BrowseContractingPartiesFragmentModule
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by MaRaT on 03.06.2018.
 */

@Subcomponent(modules = arrayOf(
        BrowseContractingPartiesFragmentModule::class
))
interface BrowseContractingPartiesFragmentSubComponent : AndroidInjector<ContractingPartiesFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<ContractingPartiesFragment>()
}