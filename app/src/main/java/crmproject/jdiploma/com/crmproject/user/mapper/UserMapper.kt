package crmproject.jdiploma.com.crmproject.user.mapper

import crmproject.jdiploma.com.crmproject.Mapper
import crmproject.jdiploma.com.crmproject.organization.mapper.OrganizationMapper
import crmproject.jdiploma.com.crmproject.role.mapper.RoleMapper
import crmproject.jdiploma.com.crmproject.user.model.UserViewModel
import crmproject.jdiploma.com.presentation.user.model.DjangoUserView
import crmproject.jdiploma.com.presentation.user.model.UserView
import javax.inject.Inject

/**
 * Created by MaRaT on 28.02.2018.
 */

class UserMapper @Inject constructor(
        private val roleMapper: RoleMapper,
        private val organizationMapper: OrganizationMapper
) : Mapper<UserViewModel, UserView> {
    override fun mapToViewModel(type: UserView): UserViewModel {
        return UserViewModel(
                type.id,
                type.djangoUser.username,
                type.djangoUser.firstName,
                type.djangoUser.lastName,
                type.djangoUser.email,
                type.djangoUser.isSuperuser,
                type.djangoUser.lastLogin,
                roleMapper.mapToViewModel(type.role),
                organizationMapper.mapToViewModel(type.organization),
                type.position
        )
    }

    override fun mapFromViewModel(type: UserViewModel): UserView {
        return UserView(
                type.id,
                DjangoUserView(
                        type.id,
                        type.username,
                        type.firstName,
                        type.lastName,
                        type.email,
                        type.isSuperuser,
                        type.lastLogin
                ),
                roleMapper.mapFromViewModel(type.role),
                organizationMapper.mapFromViewModel(type.organization),
                type.position
        )
    }

}