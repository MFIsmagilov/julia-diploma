package crmproject.jdiploma.com.crmproject.login.injection.component

import crmproject.jdiploma.com.crmproject.login.LoginActivity
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by MaRaT on 03.06.2018.
 */
@Subcomponent
interface BrowseLoginActivitySubCompoment : AndroidInjector<LoginActivity> {
    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<LoginActivity>()
}