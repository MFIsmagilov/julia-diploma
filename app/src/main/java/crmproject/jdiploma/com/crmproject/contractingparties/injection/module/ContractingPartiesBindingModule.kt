package crmproject.jdiploma.com.crmproject.contractingparties.injection.module

import crmproject.jdiploma.com.crmproject.contractingparties.ContractingPartiesFragment
import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.crmproject.tasks.DetailTaskActivity
import crmproject.jdiploma.com.crmproject.tasks.TaskFragment
import crmproject.jdiploma.com.crmproject.tasks.injection.module.BrowseTasksFragmentModule
import crmproject.jdiploma.com.crmproject.tasks.injection.module.DetailTaskActivityModule
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * Created by MaRaT on 03.06.2018.
 */
@Module
abstract class ContractingPartiesBindingModule {

    @PerActivity
    @ContributesAndroidInjector(modules = arrayOf(BrowseContractingPartiesFragmentModule::class))
    abstract fun bindCpFragment(): ContractingPartiesFragment

}