package crmproject.jdiploma.com.crmproject.contractingparties.injection.module

import crmproject.jdiploma.com.crmproject.contractingparties.ContractingPartiesFragment
import crmproject.jdiploma.com.crmproject.injection.scopes.PerActivity
import crmproject.jdiploma.com.domain.counterparty.interactor.browse.CountingPartiesBrowse
import crmproject.jdiploma.com.presentation.counterparty.browse.BrowseCounterparty
import crmproject.jdiploma.com.presentation.counterparty.browse.BrowseCounterpartyPreseter
import crmproject.jdiploma.com.presentation.counterparty.mapper.CounterpartyMapper
import dagger.Module
import dagger.Provides

/**
 * Created by MaRaT on 03.06.2018.
 */
@Module
open class BrowseContractingPartiesFragmentModule {

    @PerActivity
    @Provides
    internal fun provideBrowseView(contractingPartiesFragment: ContractingPartiesFragment): BrowseCounterparty.View {
        return contractingPartiesFragment
    }

    @PerActivity
    @Provides
    internal fun provideBrowsePresenter(mainView: BrowseCounterparty.View,
                                        contractingPartiesBrowse: CountingPartiesBrowse,
                                        mapper: CounterpartyMapper
    ):
            BrowseCounterparty.Presenter {
        return BrowseCounterpartyPreseter(mainView, contractingPartiesBrowse, mapper)
    }

}