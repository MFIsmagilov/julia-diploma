package crmproject.jdiploma.com.crmproject.user.injection.component

import crmproject.jdiploma.com.crmproject.user.browse.UsersFragment
import dagger.Subcomponent
import dagger.android.AndroidInjector

/**
 * Created by MaRaT on 15.03.2018.
 */
@Subcomponent
interface UsersFragmentSubComponent : AndroidInjector<UsersFragment> {

    @Subcomponent.Builder
    abstract class Builder : AndroidInjector.Builder<UsersFragment>()

}