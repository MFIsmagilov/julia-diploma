package crmproject.jdiploma.com.crmproject

import crmproject.jdiploma.com.crmproject.contractingparties.model.CounterpartyViewModel
import crmproject.jdiploma.com.crmproject.dealings.model.DealingViewModel
import crmproject.jdiploma.com.crmproject.organization.model.OrganizationViewModel
import crmproject.jdiploma.com.crmproject.role.model.RoleViewModel
import crmproject.jdiploma.com.crmproject.tasks.model.TaskViewModel
import crmproject.jdiploma.com.crmproject.user.model.UserViewModel
import java.util.*

/**
 * Created by MaRaT on 02.06.2018.
 */

fun getUser(): UserViewModel {
    return UserViewModel(
            25,
            "Julik",
            "Юлия",
            "Егорова",
            "julia@crm.com1",
            false,
            Calendar.getInstance().time,
            getRole(),
            getOrg(),
            "test position"
    )
}

fun getRole(): RoleViewModel {
    return RoleViewModel(1, "test role")
}

fun getOrg(): OrganizationViewModel {
    return OrganizationViewModel(20, "OOO Test", "test.exemple.crm")
}

fun getTasks(): List<TaskViewModel> {
    val calendar1 = Calendar.getInstance()
    calendar1.add(Calendar.DAY_OF_MONTH, -2)
    calendar1.add(Calendar.MONTH, -1)
    val calendar2 = Calendar.getInstance()
    calendar2.add(Calendar.DAY_OF_MONTH, -1)
    calendar2.add(Calendar.MONTH, -1)
    return arrayListOf<TaskViewModel>(
            TaskViewModel(calendar1.time, calendar2.time,
                    "Test 1", null, "Не выполнено"),
            TaskViewModel(calendar1.time, calendar2.time,
                    "Test 2", getUser(), "Не выполнено"),
            TaskViewModel(calendar1.time, calendar2.time,
                    "Test 3", getUser(), "Выполнено")
    )
}

fun getUsers(): List<UserViewModel> {
    return listOf(
            getUser(),
            UserViewModel(1, "Ivan", "Ivanov", "Ivan", "ivan@crm.com1", false, Calendar.getInstance().time, getRole(), getOrg(), "test position #1"),
            UserViewModel(2, "Maria", "Petrova", "Maria", "maria@crm.com1", false, Calendar.getInstance().time, getRole(), getOrg(), "test position #2")
    )
}

fun getContractingParties(): List<CounterpartyViewModel> {
    return listOf(
            CounterpartyViewModel(1, "Тестовый контаргент #1", "test.address1", "test.site1"),
            CounterpartyViewModel(2, "Тестовый контаргент #2", "test.address2", "test.site2")
    )
}

fun getDealings(): List<DealingViewModel> {
    return listOf(
            DealingViewModel(1, getUser(), CounterpartyViewModel(1, "", "", ""),
                    "Тестовая сделка #1", Calendar.getInstance().time, Calendar.getInstance().time, 10000),
            DealingViewModel(1, getUser(), CounterpartyViewModel(1, "", "", ""),
                    "Тестовая сделка #2", Calendar.getInstance().time, Calendar.getInstance().time, 50000))
}