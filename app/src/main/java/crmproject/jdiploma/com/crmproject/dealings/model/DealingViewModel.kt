package crmproject.jdiploma.com.crmproject.dealings.model

import crmproject.jdiploma.com.crmproject.contractingparties.model.CounterpartyViewModel
import crmproject.jdiploma.com.crmproject.organization.model.OrganizationViewModel
import crmproject.jdiploma.com.crmproject.role.model.RoleViewModel
import crmproject.jdiploma.com.crmproject.user.model.UserViewModel
import java.util.*

/**
 * Created by MaRaT on 03.06.2018.
 */
class DealingViewModel(
        val id: Int,
        val user: UserViewModel,
        val counterparty: CounterpartyViewModel,
        val name: String,
        val dateBegining: Date,
        val dateCompletion: Date,
        val summa: Int
)